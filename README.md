# Grpah Demo
* This project is a major assignment in one of my courses.
  It's primary purpose is to illustrate different kinds of graph and deliver various kinds of information to users.
* To build the project, you need Visual Studio 2013 or newer version and Qt SDK.
  To run the program with best performance, please compile and run the project under "Release" mode.
