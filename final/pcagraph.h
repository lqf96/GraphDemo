#pragma once

#include <functional>
#include <qvariant.h>
#include <qfile.h>
#include <qhash.h>
#include <qregexp.h>
#include <qtextstream.h>
#include <qlist.h>
#include "graph.h"

//PCA graph class
class PCAGraph : public Graph
{public:
	//Graph loaded or not
	bool loaded;
	//Failure reason
	QString failed_reason;
	//Property name table
	QList<QString> prop_table;

	//Data parsers
	static QList<std::function<bool(QString, QVariant*)>> data_parsers;

	//Constructor
	PCAGraph(QString path);

	//PCA graph data parser initialization
	static void initDataParsers();
};