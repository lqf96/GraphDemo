#pragma once

#include <cmath>
#include <functional>
#include <climits>
#include <qhash.h>
#include <qstring.h>
#include <qsharedpointer.h>
#include "graph.h"
#include "pcagraph.h"
#include "topicgraph.h"
#include "ogdflayout.h"

//* Constants
//PI
const double PI = 3.1415926535;
//Elastic force stable length
extern const double ELASTIC_STABLE_LENGTH;
//Elastic force K value
extern const double ELASTIC_K;
//Simple Repulsive force K value
extern const double SRF_K;
extern const double SRF_RANGE;

//Fixed point default layout strategy
class FixedPointLayoutStrategy : public GraphLayoutStrategy
{private:
	//Transition information
	struct TransitionInfo
	{
		//Acceleration
		double ax;
		double ay;
		double az;
	};

	//Frame per second
	unsigned int fps;
	//Stable time
	double stable_time;

	//Transition counter
	unsigned int counter;
	//Transition information
	QHash<Node*, TransitionInfo> layout_info;
	//Is stable or not
	bool stable;
public:
	//Constructor
	FixedPointLayoutStrategy(QSharedPointer<Graph> graph, unsigned int _fps = 60, double _stable_time = 2);

	//Reset and establish layout transition state
	void newState(std::function<void(Node*, double*, double*, double*)> func);
	//Run layout strategy
	void run();
	//Graph is stable or not
	bool isStable();
};

//Fixed point layout final state function
//PCA default layout (PCA, 2D)
void PCADefaultFinalState(Node* node, double* tx, double* ty, double* tz);
//Random layout strategy (2D / 3D)
void RandomFinalState(double* tx, double* ty, double* tz, int range);
//PCA Multi-level circular layout strategy (PCA, 2D)
class PCAMultiLevelCircularFinalState
{private:
	//Has central point
	bool central_point;
	//Radius
	double radius;

	//Paper, conference and author angle
	double p_angle, c_angle, a_angle;
	//Paper, conference and author count
	unsigned int p_count, c_count, a_count;
public:
	//Constructor
	PCAMultiLevelCircularFinalState(QSharedPointer<PCAGraph> graph, double r = 80);

	//Function call entry
	void operator ()(Node* node, double* tx, double* ty, double* tz);
};
//Simple circular layout strategy (2D)
class CircularFinalState
{private:
	//Radius
	double radius;

	//Angle
	double angle;
	//Count
	unsigned int count;
public:
	//Constructor
	CircularFinalState(QSharedPointer<Graph> graph, double r = 200);

	//Function call entry
	void operator()(double* tx, double* ty, double* tz);
};

//Force directed layout strategy
class ForceDirectedLayoutStrategy : public GraphLayoutStrategy
{private:
	//Transition information
	struct TransitionInfo
	{
		//Acceleration
		double ax;
		double ay;
		double az;
		//Velocity
		double vx;
		double vy;
		double vz;
	};

	//Frame per second
	unsigned int fps;

	//Edge force function
	std::function<bool(Edge*, double*, double*, double*)> edge_force_func;
	//Node force function
	std::function<bool(Node*, Node*, double*, double*, double*)> node_force_func;

	//Transition information
	QHash<Node*, TransitionInfo> layout_info;
	//Is stable or not
	bool stable;
	//Total energy
	double total_energy;
public:
	//Constructor
	ForceDirectedLayoutStrategy(QSharedPointer<Graph> graph, unsigned int _fps = 60);

	//Reset and establish layout transition state
	//(Use nullptr instead of NULL here as NULL brings problems in C++ 11)
	void newState(std::function<bool(Edge*, double*, double*, double*)> _edge_force_func = nullptr,
		std::function<bool(Node*, Node*, double*, double*, double*)> _node_force_func = nullptr);
	//Run layout strategy
	void run();
	//Graph is stable or not
	bool isStable();

	//Get force function
	std::function<bool(Edge*, double*, double*, double*)> getEdgeForceFunc();
	std::function<bool(Node*, Node*, double*, double*, double*)> getNodeForceFunc();
};

//Force directed layout force functions
//Elastic force
bool ElasticForce(Edge* edge, double* fx, double* fy, double* fz, double stable_length, double k);
//Simple repulsive force
bool SimpleRepulsiveForce(Node* node1, Node* node2, double* fx, double* fy, double* fz, double k, double max_range);