#include "gdgraphicsview.h"

//PCA graphics node item class
//Constructor
PCAGraphicsNodeItem::PCAGraphicsNodeItem(PCAGraphicsView* _self, Node* _node)
{
	this->node = _node;
	this->self = _self;
	this->mouse_status = MouseState::NotPressed;

	//Initialize coordinate
	auto coordinate = node->prop<QList<double>>("ni_viewLayout");
	node->__prop__["x"] = coordinate[0];
	node->__prop__["y"] = coordinate[1];
	node->__prop__["z"] = coordinate[2];

	//Get node color
	QColor node_color;
	auto node_color_list = _node->prop<QList<double>>("ni_viewColor");
	node_color.setRedF(node_color_list[0] / 256.0);
	node_color.setGreenF(node_color_list[1] / 256.0);
	node_color.setBlueF(node_color_list[2] / 256.0);
	node_color.setAlphaF(node_color_list[3] / 256.0);
	//Create bounding rectangle
	QRectF node_item_rect(0, 0, 2 * NODE_ITEM_RADIUS, 2 * NODE_ITEM_RADIUS);
	//Set bounding rect and color
	this->setBrush(node_color);
	this->setPen(node_color);
	this->setRect(node_item_rect);

	//Set tooltip text
	this->setToolTip("Node #" + QString::number(this->node->prop<unsigned int>("node_id")));

	//Set Z value to make node on top of the edge
	this->setZValue(1);

	//Allow event handling
	this->setAcceptedMouseButtons(Qt::LeftButton);
	this->setAcceptHoverEvents(true);
	//Allow selecting
	this->setFlag(QGraphicsItem::ItemIsSelectable, true);

	//Add node graphics item reference and node color to node
	node->prop("__gitem", this);
	node->prop("__gcolor", node_color);

	//Set highlight handler and unset handler
	QVariant highlight_handler;
	QVariant unset_handler;

	highlight_handler.setValue<std::function<void()>>([this]()
	{
		this->setBrush(Qt::GlobalColor::white);
		this->setPen(QColor(Qt::GlobalColor::white));
	});
	unset_handler.setValue<std::function<void()>>([this]()
	{
		QColor node_color = this->node->prop<QColor>("__gcolor");
		this->setBrush(node_color);
		this->setPen(node_color);
	});

	this->setData(HIGHLIGHT_HANDLER, highlight_handler);
	this->setData(UNSET_HIGHLIGHT_HANDLER, unset_handler);
}

//Hover enter event
void PCAGraphicsNodeItem::hoverEnterEvent(QGraphicsSceneHoverEvent* e)
{
	(this->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->setCursor(Qt::PointingHandCursor);
}

//Hover leave event
void PCAGraphicsNodeItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* e)
{
	if (!this->isSelected())
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->unsetCursor();
}

//Mouse pressed event
void PCAGraphicsNodeItem::mousePressEvent(QGraphicsSceneMouseEvent* e)
{
	self->nodeSelectedWrapper(reinterpret_cast<unsigned long long>(this->node));
	this->mouse_status = MouseState::Pressed;
	
	//Current item not contained in selected items, deselect them
	auto selected_items = self->pca_scene->selectedItems();
	if (!selected_items.contains(this))
	{
		for (auto item : selected_items)
			item->setSelected(false);
		this->setSelected(true);
	}
}

//Mouse move event
void PCAGraphicsNodeItem::mouseMoveEvent(QGraphicsSceneMouseEvent * e)
{
	if (this->mouse_status == MouseState::NotPressed)
		return;

	double dx = e->scenePos().x() - e->lastScenePos().x(),
		dy = e->scenePos().y() - e->lastScenePos().y();
	for (auto item : self->pca_scene->selectedItems())
		((PCAGraphicsNodeItem*)item)->moveNode(dx, dy);

	//Free drag mode (Random triggered)
	if (self->allow_free_drag)
		if (std::rand() % FREE_DRAG_FACTOR == 0)
			self->state_reset_handler(self->layout);

	if (this->mouse_status == MouseState::Pressed)
	{
		this->setCursor(Qt::ClosedHandCursor);
		this->mouse_status = MouseState::Moved;
	}
}

//Mouse release event
void PCAGraphicsNodeItem::mouseReleaseEvent(QGraphicsSceneMouseEvent * e)
{
	this->setCursor(Qt::PointingHandCursor);
	this->mouse_status = MouseState::NotPressed;
}

//Move node
//(With dx and dy)
void PCAGraphicsNodeItem::moveNode(double dx, double dy)
{
	this->moveBy(dx, dy);
	this->node->prop("x", this->node->prop<double>("x") + dx);
	this->node->prop("y", this->node->prop<double>("y") + dy);

	//Edges in which current node is source node
	for (auto edge : this->node->getConnectedEdges())
	{
		auto edge_item = edge->prop<PCAGraphicsEdgeItem*>("__gitem");
		edge_item->moveEdge();
	}

	//Edges in which current node is dest node
	for (auto edge : this->node->getConnectedEdges(false))
	{
		auto edge_item = edge->prop<PCAGraphicsEdgeItem*>("__gitem");
		edge_item->moveEdge();
	}
}

//(Without dx and dy)
void PCAGraphicsNodeItem::moveNode()
{
	this->setPos(this->node->prop<double>("x") - NODE_ITEM_RADIUS,
		this->node->prop<double>("y") - NODE_ITEM_RADIUS);

	for (auto edge : this->node->getConnectedEdges())
	{
		auto edge_item = edge->getEffectiveEdge()->prop<PCAGraphicsEdgeItem*>("__gitem");
		edge_item->moveEdge();
	}
}

//Graphics item change
QVariant PCAGraphicsNodeItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
	//Unset highlight when the item is unselected
	if ((change == QGraphicsItem::ItemSelectedHasChanged) && (!this->isSelected()))
	{
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
		this->setData(__MULTI_SELECTED, false);

		return true;
	}
	//Other changes are handled by base class handler
	else
		return QGraphicsEllipseItem::itemChange(change, value);
}

//PCA graphics edge item class
//Constructor
PCAGraphicsEdgeItem::PCAGraphicsEdgeItem(PCAGraphicsView* _self, Edge* _edge)
{
	this->edge = _edge;
	this->self = _self;

	double x1 = _edge->getNodeA()->prop<double>("x"),
		y1 = _edge->getNodeA()->prop<double>("y"),
		x2 = _edge->getNodeB()->prop<double>("x"),
		y2 = _edge->getNodeB()->prop<double>("y");

	//Set linear gradient
	QLinearGradient gradient(x1, y1, x2, y2);
	QColor color_a = _edge->getNodeA()->prop<QColor>("__gcolor"),
		color_b = _edge->getNodeB()->prop<QColor>("__gcolor");
	//Alpha value is manually set to ensure nodes can be clear seen among edges
	color_a.setAlpha(128);
	color_b.setAlpha(128);
	gradient.setColorAt(0, color_a);
	gradient.setColorAt(1, color_b);
	//Create QPen object and apply QPen
	this->_pen = QPen(gradient, EDGE_WIDTH);
	this->setPen(this->_pen);
	//Set line
	this->setLine(x1, y1, x2, y2);

	//Set tooltip text
	this->setToolTip("Edge");

	//Allow event handling
	this->setAcceptedMouseButtons(Qt::LeftButton);
	this->setAcceptHoverEvents(true);
	//Allow selecting
	this->setFlag(QGraphicsItem::ItemIsSelectable, true);
	//Single selection flag set to false by default
	this->single_flag = false;

	//Add edge graphics item reference to edge
	this->edge->prop("__gitem", this);

	//Set highlight handler and unset handler
	QVariant highlight_handler;
	QVariant unset_handler;

	highlight_handler.setValue<std::function<void()>>([this]()
	{
		this->setPen(QColor(Qt::GlobalColor::white));
	});
	unset_handler.setValue<std::function<void()>>([this]()
	{
		this->setPen(this->_pen);
	});

	this->setData(HIGHLIGHT_HANDLER, highlight_handler);
	this->setData(UNSET_HIGHLIGHT_HANDLER, unset_handler);
}

//Hover enter event
void PCAGraphicsEdgeItem::hoverEnterEvent(QGraphicsSceneHoverEvent* e)
{
	(this->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->setCursor(Qt::PointingHandCursor);
}

//Hover leave event
void PCAGraphicsEdgeItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* e)
{
	if (!this->isSelected())
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->unsetCursor();
}

//Mouse pressed event
void PCAGraphicsEdgeItem::mousePressEvent(QGraphicsSceneMouseEvent* e)
{
	//Emit edge selected signal
	self->edgeSelectedWrapper(reinterpret_cast<unsigned long long>(this->edge));

	//Unselect other selected items
	for (auto item : self->pca_scene->selectedItems())
		item->setSelected(false);
	//Select current item
	this->single_flag = true;
	this->setSelected(true);
}

//Move edge
void PCAGraphicsEdgeItem::moveEdge()
{
	//Get endpoint coordinate
	double x1 = this->edge->getNodeA()->prop<double>("x"),
		y1 = this->edge->getNodeA()->prop<double>("y"),
		x2 = this->edge->getNodeB()->prop<double>("x"),
		y2 = this->edge->getNodeB()->prop<double>("y");
	//Redraw and set line
	QLine new_line(x1, y1, x2, y2);
	this->setLine(new_line);

	//Regenerate gradient effect
	QLinearGradient gradient(x1, y1, x2, y2);
	QColor color_a = this->edge->getNodeA()->prop<QColor>("__gcolor"),
		color_b = this->edge->getNodeB()->prop<QColor>("__gcolor");
	//Alpha value is manually set to ensure nodes can be clear seen among edges
	color_a.setAlpha(128);
	color_b.setAlpha(128);
	gradient.setColorAt(0, color_a);
	gradient.setColorAt(1, color_b);
	//Set pen
	this->setPen(QPen(gradient, EDGE_WIDTH));
}

//Graphics item change
QVariant PCAGraphicsEdgeItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
	//Item is being selected
	if ((change == QGraphicsItem::ItemSelectedChange) && value.toBool())
	{
		//Single selection state
		if (this->single_flag)
		{
			this->single_flag = false;
			return true;
		}
		//Multiple selection state, blocked
		else
			return false;
	}
	//Unset highlight when the item is unselected
	else if ((change == QGraphicsItem::ItemSelectedHasChanged) && (!this->isSelected()))
	{
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
		this->setData(__MULTI_SELECTED, false);

		return true;
	}
	//Other changes are handled by base class handler
	else
		return QGraphicsLineItem::itemChange(change, value);
}

//PCA graphics view class
//Constructor
PCAGraphicsView::PCAGraphicsView(QSharedPointer<PCAGraph> _graph, QWidget* parent) : QGraphicsView(parent)
{
	this->graph = _graph;

	//Initialize layout transition timer
	this->layout_timer = new QTimer(this);
	this->layout_timer->setInterval(1000 / FPS);
	connect(layout_timer, SIGNAL(timeout()), this, SLOT(layoutTransitionHandler()));

	//Initialize layout reference
	this->layout = NULL;

	//Init 2D environment
	this->init2D();
}

//2D environment initialization
void PCAGraphicsView::init2D()
{
	//Set up scene
	this->pca_scene = new QGraphicsScene(this);
	this->setScene(pca_scene);
	//Set background
	pca_scene->setBackgroundBrush(Qt::GlobalColor::black);

	//Set scale status
	this->scale_status = 0;
	this->scale_at_view_side = false;

	//Set drag mode
	this->setDragMode(QGraphicsView::ScrollHandDrag);

	for (auto node : this->graph->getNodes())
	{
		//Create PCA graphics node item and add to scene
		PCAGraphicsNodeItem* node_item = new PCAGraphicsNodeItem(this, node);
		pca_scene->addItem(node_item);

		//Set node graphics item position
		//(Cannot be done in node item constructor as this will cause coordinate problem)
		node_item->setPos(node->prop<double>("x"), node->prop<double>("y"));
	}

	for (auto edge : this->graph->getEdges())
	{
		//Create PCA graphics edge item and add to scene
		PCAGraphicsEdgeItem* edge_item = new PCAGraphicsEdgeItem(this, edge);
		pca_scene->addItem(edge_item);
	}

	//Connect signals and slots
	connect(this, SIGNAL(rubberBandChanged(QRect, QPointF, QPointF)), this, SLOT(rubberBandChangeHandler(QRect, QPointF, QPointF)));
}

//Mouse wheel event
void PCAGraphicsView::wheelEvent(QWheelEvent* e)
{
	double ds = e->delta() / SCALE_FACTOR;
	if ((this->scale_status + ds > UPPER_SCALE_RANGE) || (this->scale_status + ds < LOWER_SCALE_RANGE))
		return;
	this->scale(std::exp(ds), std::exp(ds));
	this->scale_status += ds;

	this->scale_at_view_side = true;
	emit scaleChanged(this->scale_status);
}

//Scale change handler
void PCAGraphicsView::scaleChangeHandler(double scale_value)
{
	if (this->scale_at_view_side)
	{
		this->scale_at_view_side = false;
		return;
	}

	this->scale(std::exp(scale_value - this->scale_status), std::exp(scale_value - this->scale_status));
	this->scale_status = scale_value;
}

//Apply layout
void PCAGraphicsView::applyLayout(QString _layout_name, bool _allow_free_drag)
{
	using namespace std::placeholders;

	GraphLayoutStrategy* new_layout;
	GraphLayoutStrategy* old_layout = this->layout;
	this->allow_free_drag = _allow_free_drag;

	//Default layout
	if (_layout_name == "Default")
	{
		FixedPointLayoutStrategy* default_layout = new FixedPointLayoutStrategy(this->graph);
		default_layout->newState(PCADefaultFinalState);
		new_layout = default_layout;
	}
	//Random layout
	else if (_layout_name == "Random")
	{
		FixedPointLayoutStrategy* random_layout = new FixedPointLayoutStrategy(this->graph);
		random_layout->newState(std::bind(RandomFinalState, _2, _3, _4, RND_RANGE));
		new_layout = random_layout;
	}
	//Circular layout
	else if (_layout_name == "Circular")
	{
		FixedPointLayoutStrategy* circular_layout = new FixedPointLayoutStrategy(this->graph);
		circular_layout->newState(std::bind(CircularFinalState(this->graph), _2, _3, _4));
		new_layout = circular_layout;
	}
	//Multi-level circular layout
	else if (_layout_name == "MultiLevelCircular")
	{
		FixedPointLayoutStrategy* circular_x_layout = new FixedPointLayoutStrategy(this->graph);
		circular_x_layout->newState(PCAMultiLevelCircularFinalState(this->graph));
		new_layout = circular_x_layout;
	}
	//OGDF FMMM layout
	else if (_layout_name == "OGDFFMMM")
	{
		FixedPointLayoutStrategy* ogdf_fmmm_layout = new FixedPointLayoutStrategy(this->graph);
		ogdf_fmmm_layout->newState(OGDFLayoutFinalState(this->graph, OGDFLayoutFinalState::OGDFLayout::FMMMLayout, PCA_FMMM_SCALE_FACTOR));
		new_layout = ogdf_fmmm_layout;
	}
	//OGDF tutte layout
	else if (_layout_name == "OGDFTutte")
	{
		FixedPointLayoutStrategy* ogdf_tutte_layout = new FixedPointLayoutStrategy(this->graph);
		ogdf_tutte_layout->newState(OGDFLayoutFinalState(this->graph, OGDFLayoutFinalState::OGDFLayout::TutteLayout, PCA_TUTTE_SCALE_FACTOR));
		new_layout = ogdf_tutte_layout;
	}
	//Elastic force directed layout
	else if (_layout_name == "ElasticForce")
	{
		auto elastic_force_layout = new ForceDirectedLayoutStrategy(this->graph);
		elastic_force_layout->newState(std::bind(ElasticForce, _1, _2, _3, _4, ELASTIC_STABLE_LENGTH, ELASTIC_K));
		new_layout = elastic_force_layout;

		//Free drag layout reset handler
		this->state_reset_handler = [](GraphLayoutStrategy* _layout)
		{
			auto layout = (ForceDirectedLayoutStrategy*)_layout;
			auto nfc = layout->getNodeForceFunc();
			auto efc = layout->getEdgeForceFunc();

			//Reset and start a new state
			layout->newState(efc, nfc);
		};
	}

	this->layout = new_layout;
	if (old_layout != NULL)
		delete old_layout;

	//Start timer
	this->layout_timer->start();
}

//Layout transition handler
void PCAGraphicsView::layoutTransitionHandler()
{
	this->layout->run();
	if (this->layout->isStable())
		this->layout_timer->stop();

	for (auto node : this->graph->getNodes())
	{
		auto node_item = node->prop<PCAGraphicsNodeItem*>("__gitem");
		node_item->moveNode();
	}
}

//Key press event
void PCAGraphicsView::keyPressEvent(QKeyEvent* e)
{
	//Ctrl pressed, enter rubber band mode
	if (e->key() == Qt::Key::Key_Control)
		this->setDragMode(DragMode::RubberBandDrag);
}

//Key release event
void PCAGraphicsView::keyReleaseEvent(QKeyEvent* e)
{
	//Ctrl pressed, exit rubber band mode
	if (e->key() == Qt::Key::Key_Control)
		this->setDragMode(DragMode::ScrollHandDrag);
}

//Rubber band change handler
void PCAGraphicsView::rubberBandChangeHandler(QRect, QPointF, QPointF)
{
	for (auto item : this->pca_scene->selectedItems())
		if (item->data(__MULTI_SELECTED) != true)
		{
			(item->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
			item->setData(__MULTI_SELECTED, true);
		}
}

//Highlight node
void PCAGraphicsView::highlightNode(unsigned long long _node)
{
	for (auto item : this->pca_scene->selectedItems())
		item->setSelected(false);

	auto node = reinterpret_cast<Node*>(_node);
	auto node_item = node->prop<PCAGraphicsNodeItem*>("__gitem");
	(node_item->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	node_item->setSelected(true);
}

//Qt signal wrapper
void PCAGraphicsView::nodeSelectedWrapper(unsigned long long _node){ emit nodeSelected(_node); }
void PCAGraphicsView::edgeSelectedWrapper(unsigned long long _edge){ emit edgeSelected(_edge); }

//Qt meta type registration
Q_DECLARE_METATYPE(QGraphicsEllipseItem*)
Q_DECLARE_METATYPE(QGraphicsLineItem*)
Q_DECLARE_METATYPE(PCAGraphicsNodeItem*)
Q_DECLARE_METATYPE(PCAGraphicsEdgeItem*)
Q_DECLARE_METATYPE(std::function<void()>)