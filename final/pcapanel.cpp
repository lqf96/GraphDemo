#include "gdpanel.h"

//Constants
//Welcome message
const QString WELCOME_MSG = "<p style='line-height: 20px; padding: 0px;'>"
	"Haven't got started, right?<br />"
	"Click on a node to see details.</p>";
//Node info panel title
const QString NODE_TITLE = "Node #%1";
//Edge info message
const QString EDGE_MSG = "<p style='line-height: 20px; padding: 0px;'>"
	"This edge connects node #%1 (Source) and node #%2 (Dest).</p>";
//Edge panel go to node message
const QString GO_TO_NODE = "Go to node #%1";
//Search condition parser state
const unsigned int COND_QUALIFIED = 0;
const unsigned int COND_NOT_QUALIFIED = 1;
const unsigned int TYPE_NOT_MATCH = 2;
//Search input illegal prompt
const QString ILLEGAL_SEARCH_COND = "Illegal search condition found for property '%1'.";

//PCA graph panel class
//Static variables
//[Search panel] Condition parsers
QList<std::function<QVariant(Node*, QString, QString)>> PCAPanel::cond_parsers;

//Constructor
PCAPanel::PCAPanel(QSharedPointer<PCAGraph> _graph)
{
	//* Initialize components
	//Global components
	this->panel_mode = new QPushButton(TO_SEARCH_MODE);
	this->view_slider = new QSlider(Qt::Horizontal);
	this->display_mode = new QComboBox();
	this->layout_strategy_combo = new QComboBox();
	//Welcome panel
	this->welcome_panel = new QWidget();
	this->welcome_title = new QLabel("Welcome");
	this->welcome_msg = new QLabel(WELCOME_MSG);
	//Node info panel
	this->node_info_panel = new QWidget();
	this->node_title = new QLabel(NODE_TITLE);
	this->node_type = new QLabel("[Type]");
	this->prop_table = new QTableWidget(0, 2);
	//Edge info panel
	this->edge_info_panel = new QWidget();
	this->edge_title = new QLabel("Edge");
	this->edge_msg = new QLabel(EDGE_MSG);
	this->go_to_node1 = new QPushButton(GO_TO_NODE);
	this->go_to_node2 = new QPushButton(GO_TO_NODE);
	//Search panel
	this->search_panel = new QWidget();
	this->search_title = new QLabel("Search");
	this->search_cond_table = new QTableWidget(0, 2);
	this->new_cond_btn = new QPushButton("Add");
	this->clear_cond_btn = new QPushButton("Clear");
	this->search_btn = new QPushButton("Search");
	this->search_result = new QListWidget();

	//* Component configuration
	//Global components
	this->panel_mode->setFixedWidth(60);
	this->view_slider->setRange((int)(_LOWER_SCALE_RANGE * SLIDER_FACTOR), (int)(_UPPER_SCALE_RANGE * SLIDER_FACTOR));
	this->display_mode->setFixedWidth(50);
	this->display_mode->addItem("2D");
	this->display_mode->addItem("3D");
	this->layout_strategy_combo->addItem("Default", makeQtTuple(QString("Default"), false));
	this->layout_strategy_combo->addItem("Random", makeQtTuple(QString("Random"), false));
	this->layout_strategy_combo->addItem("Circular", makeQtTuple(QString("Circular"), false));
	this->layout_strategy_combo->addItem("Circular+", makeQtTuple(QString("MultiLevelCircular"), false));
	this->layout_strategy_combo->addItem("(OGDF) FMMM", makeQtTuple(QString("OGDFFMMM"), false));
	this->layout_strategy_combo->addItem("(OGDF) Tutte", makeQtTuple(QString("OGDFTutte"), false));
	this->layout_strategy_combo->addItem("Force (Elastic)", makeQtTuple(QString("ElasticForce"), true));
	//Welcome panel components
	this->welcome_title->setFont(BSUI::BSUIFont(24));
	this->welcome_title->setPalette(BSUI::BSUIFontColor());
	this->welcome_msg->setFont(BSUI::BSUIFont(14));
	this->welcome_msg->setPalette(BSUI::BSUIFontColor());
	this->welcome_msg->setWordWrap(true);
	//Node info panel
	this->node_title->setFont(BSUI::BSUIFont(24));
	this->node_title->setPalette(BSUI::BSUIFontColor());
	QList<QString> prop_table_headers;
	prop_table_headers.append("Property");
	prop_table_headers.append("Value");
	this->prop_table->setHorizontalHeaderLabels(prop_table_headers);
	this->prop_table->verticalHeader()->setVisible(false);
	this->prop_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	//Edge info panel
	this->edge_title->setFont(BSUI::BSUIFont(24));
	this->edge_title->setPalette(BSUI::BSUIFontColor());
	this->edge_msg->setFont(BSUI::BSUIFont(14));
	this->edge_msg->setPalette(BSUI::BSUIFontColor());
	this->edge_msg->setWordWrap(true);
	this->go_to_node1->setMaximumWidth(150);
	this->go_to_node2->setMaximumWidth(150);
	//Search panel
	this->search_title->setFont(BSUI::BSUIFont(24));
	this->search_title->setPalette(BSUI::BSUIFontColor());
	QList<QString> cond_table_headers;
	cond_table_headers.append("Property");
	cond_table_headers.append("Value");
	this->search_cond_table->setHorizontalHeaderLabels(cond_table_headers);
	this->search_cond_table->verticalHeader()->setVisible(false);

	//* Create layout and assemble components
	//Global layout
	global_layout = new QVBoxLayout();
	QHBoxLayout* global_line1 = new QHBoxLayout();
	QHBoxLayout* global_line2 = new QHBoxLayout();
	//Welcome panel layout
	QVBoxLayout* welcome_layout = new QVBoxLayout();
	//Assemble welcome panel
	welcome_layout->setMargin(0);
	welcome_layout->addSpacing(10);
	welcome_layout->addWidget(this->welcome_title);
	welcome_layout->addWidget(this->welcome_msg);
	welcome_layout->addStretch(1);
	//Node info panel layout
	QGridLayout* node_layout = new QGridLayout();
	//Assemble node info panel
	node_layout->setMargin(0);
	node_layout->addItem(new QSpacerItem(0, 10), 0, 0, 1, 2);
	node_layout->addWidget(this->node_title, 1, 0);
	node_layout->addWidget(this->node_type, 1, 1, Qt::AlignRight);
	node_layout->addWidget(this->prop_table, 2, 0, 1, 2);
	//Edge info panel layout
	QVBoxLayout* edge_layout = new QVBoxLayout();
	//Assemble edge info panel
	edge_layout->setMargin(0);
	edge_layout->addSpacing(10);
	edge_layout->addWidget(this->edge_title);
	edge_layout->addWidget(this->edge_msg);
	edge_layout->addWidget(this->go_to_node1);
	edge_layout->addWidget(this->go_to_node2);
	edge_layout->addStretch(1);
	//Search panel layout
	QGridLayout* search_layout = new QGridLayout();
	//Assemble search panel
	search_layout->setMargin(0);
	search_layout->addItem(new QSpacerItem(0, 10), 0, 0, 1, 2);
	search_layout->addWidget(this->search_title, 1, 0, 1, 2);
	search_layout->addWidget(this->search_cond_table, 2, 0, 1, 2);
	search_layout->addWidget(this->new_cond_btn, 3, 0);
	search_layout->addWidget(this->clear_cond_btn, 3, 1);
	search_layout->addWidget(this->search_btn, 4, 0, 1, 2);
	search_layout->addWidget(this->search_result, 5, 0, 1, 2);
	search_layout->setRowStretch(2, 1);
	search_layout->setRowStretch(5, 1);
	//Apply panel layout
	this->welcome_panel->setLayout(welcome_layout);
	this->node_info_panel->setLayout(node_layout);
	this->edge_info_panel->setLayout(edge_layout);
	this->search_panel->setLayout(search_layout);
	//Assemble global components
	global_line1->addWidget(this->panel_mode);
	global_line1->addWidget(this->view_slider, 1);
	global_line2->addWidget(this->display_mode);
	global_line2->addWidget(this->layout_strategy_combo, 1);
	this->global_layout->addLayout(global_line1);
	this->global_layout->addLayout(global_line2);
	this->global_layout->addWidget(welcome_panel);
	//Apply global layout
	this->setLayout(this->global_layout);

	//* Connect signals and slots
	//Global components
	connect(this->panel_mode, SIGNAL(clicked()), this, SLOT(switchMode()));
	connect(this->view_slider, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChangeWrapper(int)));
	connect(this->layout_strategy_combo, SIGNAL(activated(int)), this, SLOT(layoutChangeWrapper(int)));
	//Edge info panel
	connect(this->go_to_node1, SIGNAL(clicked()), this, SLOT(goToNodeA()));
	connect(this->go_to_node2, SIGNAL(clicked()), this, SLOT(goToNodeB()));
	//Search panel
	connect(this->search_btn, SIGNAL(clicked()), this, SLOT(searchNodes()));
	connect(this->clear_cond_btn, SIGNAL(clicked()), this, SLOT(clearSearchCond()));
	connect(this->new_cond_btn, SIGNAL(clicked()), this, SLOT(addSearchCond()));
	connect(this->search_result, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(showNodeInfoInSearch(QListWidgetItem*)));

	//* Other works
	//Set current panel
	this->current_panel = this->welcome_panel;
	//Set graph reference
	this->graph = _graph;
	//Normal mode by default
	this->search_mode = false;
}

//Destructor
PCAPanel::~PCAPanel()
{
	this->current_panel->setParent(NULL);
	delete this->welcome_panel;
	delete this->search_panel;
	delete this->node_info_panel;
	delete this->edge_info_panel;
}

//Slots
//Show node information
void PCAPanel::showNodeInfo(unsigned long long _node)
{
	Node* node = reinterpret_cast<Node*>(_node);
	QRegExp prop_name_rx("ni_(.*)");

	//Set node info panel
	this->node_title->setText(NODE_TITLE.arg(node->prop<unsigned int>("node_id")));
	this->node_type->setText(node->prop<QString>("ni_type"));
	//Reset property table
	int row_amount = this->prop_table->rowCount();
	int i;
	for (i = row_amount - 1; i >= 0; i--)
		this->prop_table->removeRow(i);

	row_amount = 0;
	for (auto item : node->__prop__.keys())
	{
		int pos = prop_name_rx.indexIn(item);
		QString prop_name;
		QVariant prop_data;
		QString real_prop_data;

		if (pos == -1)
			continue;

		prop_name = prop_name_rx.cap(1);
		prop_data = node->__prop__[item];

		//Number array
		if (prop_data.canConvert<QList<double>>())
		{
			QList<double> num_array = prop_data.value<QList<double>>();
			unsigned int i;

			real_prop_data = "[";
			for (i = 0; i < num_array.size() - 1; i++)
				real_prop_data += QString::number(num_array[i]) + ", ";
			real_prop_data += QString::number(num_array[num_array.size() - 1]) + "]";
		}
		//Number & string (Fallback?)
		else if (prop_data.canConvert<QString>())
			real_prop_data = prop_data.toString();
		//Failed to read data, show error message
		else
			real_prop_data = "<Error>";

		this->prop_table->insertRow(row_amount);
		this->prop_table->setItem(row_amount, 0, new QTableWidgetItem(prop_name));
		QLineEdit* prop_value_text = new QLineEdit(real_prop_data);
		prop_value_text->setReadOnly(true);
		this->prop_table->setCellWidget(row_amount, 1, prop_value_text);
		row_amount++;
	}
	
	this->global_layout->removeWidget(this->current_panel);
	this->current_panel->setParent(NULL);
	this->global_layout->addWidget(this->node_info_panel);
	this->current_panel = this->node_info_panel;

	//Extra work for search mode
	if (this->search_mode)
	{
		this->search_mode = false;
		this->panel_mode->setText(TO_SEARCH_MODE);
		this->panel_mode->adjustSize();
	}
}

//Show edge information
void PCAPanel::showEdgeInfo(unsigned long long _edge)
{
	Edge* edge = reinterpret_cast<Edge*>(_edge);
	unsigned int node_a_id, node_b_id;

	this->node_a = edge->getNodeA();
	this->node_b = edge->getNodeB();
	node_a_id = this->node_a->prop<unsigned int>("node_id");
	node_b_id = this->node_b->prop<unsigned int>("node_id");

	this->edge_msg->setText(EDGE_MSG.arg(node_a_id).arg(node_b_id));
	this->go_to_node1->setText(GO_TO_NODE.arg(node_a_id));
	this->go_to_node2->setText(GO_TO_NODE.arg(node_b_id));

	this->global_layout->removeWidget(this->current_panel);
	this->current_panel->setParent(NULL);
	this->global_layout->addWidget(this->edge_info_panel);
	this->current_panel = this->edge_info_panel;

	//Extra work for search mode
	if (this->search_mode)
	{
		this->search_mode = false;
		this->panel_mode->setText(TO_SEARCH_MODE);
		this->panel_mode->adjustSize();
	}
}

//[Edge info panel] Go to node A
void PCAPanel::goToNodeA()
{
	this->showNodeInfo(reinterpret_cast<unsigned long long>(this->node_a));
}

//[Edge info panel] Go to node B
void PCAPanel::goToNodeB()
{
	this->showNodeInfo(reinterpret_cast<unsigned long long>(this->node_b));
}

//[Global components] Scale change handler
void PCAPanel::scaleChangeHandler(double scale_value)
{
	this->view_slider->setValue((int)(scale_value * SLIDER_FACTOR));
}

//[Global components] Slider value change wrapper
void PCAPanel::sliderValueChangeWrapper(int value)
{
	emit scaleChanged((double)value / SLIDER_FACTOR);
}

//[Global components] Layout change wrapper
void PCAPanel::layoutChangeWrapper(int index)
{
	QTuple layout_tuple = this->layout_strategy_combo->itemData(index).value<QTuple>();
	emit layoutChanged(layout_tuple[0].toString(), layout_tuple[1].toBool());
}

//[Global components] Switch between normal mode and search mode
void PCAPanel::switchMode()
{
	//Switch to normal mode
	if (this->search_mode)
	{
		this->search_mode = false;

		this->global_layout->removeWidget(this->current_panel);
		this->current_panel->setParent(NULL);
		this->global_layout->addWidget(this->previous_panel);
		this->current_panel = this->previous_panel;

		this->panel_mode->setText(TO_SEARCH_MODE);
		this->panel_mode->adjustSize();
	}
	//Switch to search mode
	else
	{
		this->search_mode = true;
		//Store current panel
		this->previous_panel = this->current_panel;

		this->global_layout->removeWidget(this->current_panel);
		this->current_panel->setParent(NULL);
		this->global_layout->addWidget(this->search_panel);
		this->current_panel = this->search_panel;

		this->panel_mode->setText(TO_NORMAL_MODE);
		this->panel_mode->adjustSize();
	}
}

//[Search panel] Initialize Condition parsers
void PCAPanel::initCondParsers()
{
	//Number array
	PCAPanel::cond_parsers.append([](Node* node, QString prop, QString cond) -> QVariant
	{
		QVariant prop_value;
		//"node_id" property has no prefix!
		if (prop != "node_id")
			prop_value = node->__prop__["ni_" + prop];
		else
			prop_value = node->__prop__["node_id"];

		if (prop_value.canConvert<QList<double>>())
		{
			QList<double> prop_array = prop_value.value<QList<double>>();
			bool success;
			double _cond = cond.toDouble(&success);
			
			if (success)
				return prop_array.contains(_cond) ? COND_QUALIFIED : COND_NOT_QUALIFIED;
			else
				return ILLEGAL_SEARCH_COND.arg(prop);
		}
		else
			return TYPE_NOT_MATCH;
	});
	//Number
	PCAPanel::cond_parsers.append([](Node* node, QString prop, QString cond) -> QVariant
	{
		QVariant prop_value;
		//"node_id" property has no prefix!
		if (prop != "node_id")
			prop_value = node->__prop__["ni_" + prop];
		else
			prop_value = node->__prop__["node_id"];
		
		if ((prop_value.type() == QVariant::Double) || (prop_value.type() == QVariant::UInt))
		{
			double real_prop_value = prop_value.toDouble();
			bool success;
			double _cond = cond.toDouble(&success);

			if (success)
				return (real_prop_value == _cond) ? COND_QUALIFIED : COND_NOT_QUALIFIED;
			else
				return ILLEGAL_SEARCH_COND.arg(prop);
		}
		else
			return TYPE_NOT_MATCH;
	});
	//String (Fallback)
	PCAPanel::cond_parsers.append([](Node* node, QString prop, QString cond) -> QVariant
	{
		QString rx_special_chars = "*.?+$^[](){}|/\\";
		QString rx_legal_cond;

		auto cond_ptr = cond.begin();
		while (cond_ptr != cond.end())
		{
			if (rx_special_chars.contains(*cond_ptr))
				rx_legal_cond += '\\';
			rx_legal_cond += *cond_ptr;
			cond_ptr++;
		}

		QRegExp search_rx("^.*" + rx_legal_cond + ".*$");
		QString prop_value = node->prop<QString>("ni_" + prop);

		return (search_rx.exactMatch(prop_value)) ? COND_QUALIFIED : COND_NOT_QUALIFIED;
	});
}

//[Search panel] Add a search condition
void PCAPanel::addSearchCond()
{
	unsigned int row_count = this->search_cond_table->rowCount();

	//Insert a new row and add components
	this->search_cond_table->insertRow(row_count);
	//Add components
	auto prop_combo = new QComboBox();
	for (auto prop : this->graph->prop_table)
		prop_combo->addItem(prop);
	this->search_cond_table->setCellWidget(row_count, 0, prop_combo);
	this->search_cond_table->setCellWidget(row_count, 1, new QLineEdit());
}

//[Search panel] Clear search conditions
void PCAPanel::clearSearchCond()
{
	unsigned int row_count = this->search_cond_table->rowCount();
	while (row_count > 0)
	{
		this->search_cond_table->removeRow(row_count - 1);
		row_count--;
	}
}

//[Search panel] Search for nodes
void PCAPanel::searchNodes()
{
	QList<QPair<QString, QList<QString>>> search_cond;
	unsigned int row_count = this->search_cond_table->rowCount();
	unsigned int i;
	bool search_failed = false;
	QString failed_msg;

	//Preprocess search condition
	for (i = 0; i < row_count; i++)
	{
		auto cond_prop_combo = (QComboBox*)(this->search_cond_table->cellWidget(i, 0));
		QString cond_prop = cond_prop_combo->itemText(cond_prop_combo->currentIndex());
		QString raw_cond = ((QLineEdit*)(this->search_cond_table->cellWidget(i, 1)))->text();

		search_cond.append(QPair<QString, QList<QString>>(cond_prop, raw_cond.split(' ', QString::SkipEmptyParts)));
	}

	//Search for nodes
	auto result_list = this->graph->getNodes([&search_cond, &search_failed, &failed_msg](Node* node)
	{
		if (search_failed)
			return false;

		bool result = true;
		for (auto cond_pair : search_cond)
		{
			bool tmp_result = false;
			for (auto cond : cond_pair.second)
			{
				QVariant single_result;
				for (auto cond_parser : PCAPanel::cond_parsers)
				{
					single_result = cond_parser(node, cond_pair.first, cond);

					if (single_result.type() == QVariant::UInt)
					{
						if (single_result == COND_QUALIFIED)
						{
							tmp_result = tmp_result || true;
							break;
						}
						else if (single_result == COND_NOT_QUALIFIED)
						{
							tmp_result = tmp_result || false;
							break;
						}
					}
					else
					{
						search_failed = true;
						failed_msg = single_result.toString();
						return false;
					}
				}
			}
			result = result && tmp_result;
		}

		return result;
	});

	//Show node ID as result if succeeded
	if (!search_failed)
	{
		this->search_result->clear();
		for (auto node : result_list)
		{
			auto result_item = new QListWidgetItem("#" + QString::number(node->prop<unsigned int>("node_id")));
			result_item->setData(SEARCH_RESULT_NODE_REF, reinterpret_cast<unsigned long long>(node));
			this->search_result->addItem(result_item);
		}
	}
	//Failed, prompt and exit
	else
		QMessageBox::information(NULL, "Search Failed", "Error occured when attempting to search.\n" + failed_msg);
}

//[Search panel] Show node information
void PCAPanel::showNodeInfoInSearch(QListWidgetItem* item)
{
	unsigned long long _node = item->data(SEARCH_RESULT_NODE_REF).toULongLong();
	this->showNodeInfo(_node);
	emit highlightNode(_node);
}