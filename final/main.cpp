#include <qapplication.h>
#include "graphdemo.h"

//Program entry
int main(int argc, char *argv[])
{
	//Qt application object
	QApplication a(argc, argv);
	//Main window
	GraphDemo w;

	//Show main window and enter event loop
	w.show();
	return a.exec();
}
