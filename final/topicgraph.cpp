#include "topicgraph.h"

//Constructor
TopicGraph::TopicGraph(QString path) : Graph(false)
{
	QFile nodes_file(path + "/Nodes.txt"),
		edges_file(path + "/Edges.txt"),
		docs_file(path + "/DocumentContent.txt");
	if ((!nodes_file.exists()) || (!edges_file.exists()) || (!docs_file.exists()))
	{
		this->loaded = false;
		this->failed_reason = "Corresponding file not found.";
		return;
	}

	if (!nodes_file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		this->loaded = false;
		this->failed_reason = "Can not open nodes record file.";
		return;
	}

	QTextStream nodes_in(&nodes_file);
	QString line = nodes_in.readLine();
	while ((!nodes_in.atEnd()) && (line != ""))
	{
		Node* node = new Node();
		QRegExp line_regexp("^[^:]*: (.*)$");
		bool success;
		QString prop_data;
		int pos;

		unsigned int topic_id = line.toInt(&success);
		if (!success)
		{
			this->loaded = false;
			this->failed_reason = "The nodes record file is broken.";
			delete node;
			return;
		}
		node->__prop__["topic_id"] = topic_id;

		line = nodes_in.readLine();
		pos = line_regexp.indexIn(line);
		if (pos == -1)
		{
			this->loaded = false;
			this->failed_reason = "The nodes record file is broken.";
			delete node;
			return;
		}
		prop_data = line_regexp.cap(1);
		QList<QString> topic_words = prop_data.split(' ', QString::SkipEmptyParts);
		node->prop("topicWords", topic_words);

		line = nodes_in.readLine();
		pos = line_regexp.indexIn(line);
		if (pos == -1)
		{
			this->loaded = false;
			this->failed_reason = "The nodes record file is broken.";
			delete node;
			return;
		}
		prop_data = line_regexp.cap(1);
		QList<QString> _doc_id_list = prop_data.split(' ', QString::SkipEmptyParts);
		QList<unsigned int> doc_id_list;
		for (auto _doc_id : _doc_id_list)
		{
			unsigned int doc_id = _doc_id.toUInt(&success);
			if (!success)
			{
				this->loaded = false;
				this->failed_reason = "The nodes record file is broken.";
				delete node;
				return;
			}
			doc_id_list.append(doc_id);
		}
		node->prop("topicDocuments", doc_id_list);
		this->addNode(node);

		//Skip a blank line
		nodes_in.readLine();
		line = nodes_in.readLine();
	}
	nodes_file.close();

	if (!edges_file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		this->loaded = false;
		this->failed_reason = "Can not open edges record file.";
		return;
	}

	QTextStream edges_in(&edges_file);
	line = edges_in.readLine();
	while ((!edges_in.atEnd()) && (line != ""))
	{
		unsigned int source_id, dest_id;
		double weight;
		Node* source_node;
		Node* dest_node;
		QRegExp line_regexp("^(\\d+) (\\d+) (0\\.\\d+)$");
		int pos;

		pos = line_regexp.indexIn(line);
		if (pos == -1)
		{
			this->loaded = false;
			this->failed_reason = "The edges record file is broken.";
			return;
		}

		source_id = line_regexp.cap(1).toUInt();
		dest_id = line_regexp.cap(2).toUInt();
		weight = line_regexp.cap(3).toDouble();

		for (Node* node : this->getNodes())
		{
			if (node->__prop__["topic_id"] == source_id)
				source_node = node;
			else if (node->__prop__["topic_id"] == dest_id)
				dest_node = node;
		}

		Edge* main_edge = source_node->connect(dest_node);
		main_edge->__prop__["weight"] = weight;

		line = edges_in.readLine();
	}
	edges_file.close();

	if (!docs_file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		this->loaded = false;
		this->failed_reason = "Can not open document content file.";
		return;
	}

	QTextStream docs_in(&docs_file);
	line = docs_in.readLine();
	while ((!docs_in.atEnd()) && (line != ""))
	{
		unsigned int doc_id;
		Document new_doc;
		bool success;

		doc_id = line.toUInt(&success);
		if (!success)
		{
			this->loaded = false;
			this->failed_reason = "The document content file is broken.";
			return;
		}

		new_doc.title = docs_in.readLine();
		new_doc.content = docs_in.readLine();
		//Add document
		this->documents[doc_id] = new_doc;

		//Skip a blank line
		docs_in.readLine();
		line = docs_in.readLine();
	}
	docs_file.close();

	this->loaded = true;
}