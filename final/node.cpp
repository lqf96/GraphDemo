#include "graph.h"

//Abstract node class
//Constructor
Node::Node()
{
	this->graph = 0;
}

//Get graph reference
Graph* Node::getGraph()
{
	return this->graph;
}

//Connect to another node
Edge* Node::connect(Node* node, Edge** p_edge2)
{
	return this->graph->connect(this, node, p_edge2);
}

//Get connected edges
QList<Edge*> Node::getConnectedEdges(bool source, std::function<bool(Edge*)> func)
{
	QList<Edge*> result;

	unsigned int index;
	unsigned int i;
	for (i = 0; i < this->graph->nodes.size(); i++)
		if (this->graph->nodes[i] == this)
		{
			index = i;
			break;
		}

	if (this->graph->directed && (!source))
	{
		for (i = 0; i < this->graph->nodes.size(); i++)
			if ((this->graph->adj_matrix[i][index] != NULL) && func(this->graph->adj_matrix[i][index]))
				result.append(this->graph->adj_matrix[i][index]);
	}
	else
		for (Edge* edge : this->graph->adj_matrix[index])
			if ((edge != NULL) && func(edge))
				result.append(edge);

	return result;
}

//Get connected nodes
QList<Node*> Node::getConnectedNodes(bool source, std::function<bool(Node*)> func)
{
	QList<Edge*> _result;
	QList<Node*> result;

	if (this->getGraph()->isDirected() && (!source))
	{
		_result = this->getConnectedEdges(source, [func](Edge* edge)
		{
			return func(edge->getNodeB());
		});

		for (Edge* edge : _result)
			result.append(edge->getNodeB());
	}
	else
	{
		_result = this->getConnectedEdges(source, [func](Edge* edge)
		{
			return func(edge->getNodeA());
		});

		for (Edge* edge : _result)
			result.append(edge->getNodeA());
	}

	return result;
}