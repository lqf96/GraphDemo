#include "graphdemo.h"

//* Constants
//Welcome screen message
const QString WELCOME_MESSAGE = "Graph Demo is a useful tool for demostrating and editing academic-related graphs. Get started now!";
//About message
const QString ABOUT_STRING = "Graph Demo v1.0.0 by Qifan Lu.\n"
	"Program published under MIT license.\n"
	"Other dependent libraries are published under their respective licenses.";
//Graph file not exist prompt
const QString GRAPH_FILE_NOT_EXIST = "The folder you selected does not contain %1 graph files!";
//File open error prompt
const QString FILE_OPEN_ERROR = "Error occured when opening file.\n";
//Window title
const QString WINDOW_TITLE = "Graph Demo - %1";

//Constructor
GraphDemo::GraphDemo(QWidget *parent) : QMainWindow(parent)
{	
	//* Window initialization
	//Set up UI
	this->ui.setupUi(this);
	//Set window title
	this->setWindowTitle(WINDOW_TITLE.arg("Welcome"));
	//Set background color
	QPalette p = this->palette();
	p.setColor(QPalette::Background, QColor(246, 246, 246));
	this->setPalette(p);

	//* BSUI initialization
	BSUI::BSUIInit();
	
	//* Welcome interface initialization
	//Create object
	QWidget* welcome_jumbotron = new QWidget();
	QLabel* welcome_title = new QLabel("Welcome!");
	QLabel* welcome_message = new QLabel("<p style='line-height: 30px;'>" + WELCOME_MESSAGE + "</p>");
	auto open_pca_btn = new BSUI::BSUIButton("btn-primary btn-lg", "Open PCA Graph");
	auto open_topic_btn = new BSUI::BSUIButton("btn-info btn-lg", "Open Topic Graph");
	auto exit_btn = new BSUI::BSUIButton("btn-warning btn-lg", "Exit");
	welcome_jumbotron->setObjectName("welcome_jumbotron");
	//Set size for buttons
	open_pca_btn->adjustSize();
	open_topic_btn->adjustSize();
	exit_btn->adjustSize();
	//Set font for welcome title and message
	QFont title_font, message_font;
	message_font.setFamily("'Helvetica Neue', Helvetica, Arial, sans-serif");
	message_font.setPixelSize(21);
	title_font.setFamily("'Helvetica Neue', Helvetica, Arial, sans-serif");
	title_font.setPixelSize(63);
	//Apply font and color
	p = welcome_title->palette();
	p.setColor(QPalette::WindowText, QColor(48, 48, 48));
	welcome_title->setPalette(p);
	welcome_title->setFont(title_font);
	p = welcome_message->palette();
	p.setColor(QPalette::WindowText, QColor(48, 48, 48));
	welcome_message->setPalette(p);
	welcome_message->setFont(message_font);
	//Set style sheet for jumbotron
	welcome_jumbotron->setStyleSheet(
		"QWidget#welcome_jumbotron"
		"{   border-radius: 6px;"
		"    background-color: rgb(238,238,238);"
		"}");
	//Allow word wrapping
	welcome_message->setWordWrap(true);

	//* Welcome interface layout
	//Layout object
	QVBoxLayout* inner_layout = new QVBoxLayout();
	QGridLayout* outer_layout = new QGridLayout();
	//Set content margin and spacing
	outer_layout->setContentsMargins(60, 30, 60, 30);
	outer_layout->setSpacing(20);
	inner_layout->setContentsMargins(60, 48, 60, 48);
	inner_layout->setSpacing(20);
	//Add components
	inner_layout->addWidget(welcome_title);
	inner_layout->addWidget(welcome_message);
	outer_layout->addWidget(welcome_jumbotron, 0, 0, 1, 3);
	outer_layout->addWidget(open_pca_btn, 1, 0, 1, 1, Qt::AlignLeft);
	outer_layout->addWidget(open_topic_btn, 1, 1, 1, 1, Qt::AlignHCenter);
	outer_layout->addWidget(exit_btn, 1, 2, 1, 1, Qt::AlignRight);
	outer_layout->addItem(new QSpacerItem(0, 0), 2, 0, 1, 3);
	//Set layout strech
	inner_layout->setStretch(0, 0);
	inner_layout->setStretch(1, 0);
	outer_layout->setRowStretch(0, 0);
	outer_layout->setRowStretch(1, 0);
	outer_layout->setRowStretch(2, 1);
	//Apply layout to widgets
	welcome_jumbotron->setLayout(inner_layout);
	ui.centralWidget->setLayout(outer_layout);

	//* Event handling
	//Exit button
	exit_btn->addEventListener("click", [this](void*)
	{
		this->close();
	});
	//Open PCA graph
	open_pca_btn->addEventListener("click", [this](void*)
	{
		this->openPCAGraph();
	});
	//Open topic graph
	open_topic_btn->addEventListener("click", [this](void*)
	{
		this->openTopicGraph();
	});

	//* Extra initialization works
	//Init PCA graph data parsers
	PCAGraph::initDataParsers();
	//Init random number generator
	std::srand(std::time(NULL));
	//Init PCA and topic graph search condition parsers
	PCAPanel::initCondParsers();
	TopicPanel::initCondParsers();
}

GraphDemo::~GraphDemo()
{
	
}

//Slots
//Show about
void GraphDemo::showAbout()
{
	QMessageBox::information(NULL, "About", ABOUT_STRING);
}

//Open PCA graph
void GraphDemo::openPCAGraph()
{
	//Get folder
	QString path = QFileDialog::getExistingDirectory(NULL, "Choose PCA Graph Folder");

	//No folder chosen, abort
	if (path == "")
		return;

	//Check if file exists or not
	if (!(QFile::exists(path + "/Nodes.txt") && QFile::exists(path + "/Edges.txt")))
	{
		QMessageBox::information(NULL, "Error", QString(GRAPH_FILE_NOT_EXIST).arg("PCA"));
		return;
	}

	//Open a graph
	QSharedPointer<PCAGraph> pca_graph(new PCAGraph(path));
	//Open failed, prompt message and abort
	if (!pca_graph->loaded)
	{
		QMessageBox::information(NULL, "Error", FILE_OPEN_ERROR + pca_graph->failed_reason);
		return;
	}

	//Set window title
	this->setWindowTitle(WINDOW_TITLE.arg("PCA Graph - " + path));
	
	//Create new UI
	QWidget* new_central_widget = new QWidget(this);
	PCAGraphicsView* pca_view = new PCAGraphicsView(pca_graph);
	PCAPanel* pca_panel = new PCAPanel(pca_graph);
	//Set component size
	pca_view->setMinimumWidth(600);

	//PCA graph layout
	QHBoxLayout* layout = new QHBoxLayout();
	//Add components to layout
	layout->addWidget(pca_view);
	layout->addWidget(pca_panel);
	//Set layout strech and margin
	layout->setMargin(0);
	layout->setStretch(0, 3);
	layout->setStretch(1, 1);
	//Set layout
	new_central_widget->setLayout(layout);

	//Replace UI
	QWidget* old_central_widget = ui.centralWidget;
	this->setCentralWidget(new_central_widget);
	old_central_widget->setParent(NULL);
	delete old_central_widget;
	ui.centralWidget = new_central_widget;

	//Connect panel and view signals and slots
	//Node selected
	connect(pca_view, SIGNAL(nodeSelected(unsigned long long)), pca_panel, SLOT(showNodeInfo(unsigned long long)));
	//Edge selected
	connect(pca_view, SIGNAL(edgeSelected(unsigned long long)), pca_panel, SLOT(showEdgeInfo(unsigned long long)));
	//Scale changed
	connect(pca_view, SIGNAL(scaleChanged(double)), pca_panel, SLOT(scaleChangeHandler(double)));
	connect(pca_panel, SIGNAL(scaleChanged(double)), pca_view, SLOT(scaleChangeHandler(double)));
	//Layout changed
	connect(pca_panel, SIGNAL(layoutChanged(QString, bool)), pca_view, SLOT(applyLayout(QString, bool)));
	//Highlight node
	connect(pca_panel, SIGNAL(highlightNode(unsigned long long)), pca_view, SLOT(highlightNode(unsigned long long)));
}

//Open topic graph
void GraphDemo::openTopicGraph()
{
	//Get folder
	QString path = QFileDialog::getExistingDirectory(NULL, "Choose Topic Graph Folder");

	//No folder chosen, abort
	if (path == "")
		return;

	//Check if file exists or not
	if (!(QFile::exists(path + "/Nodes.txt") && QFile::exists(path + "/Edges.txt") && QFile::exists(path + "/DocumentContent.txt")))
	{
		QMessageBox::information(NULL, "Error", QString(GRAPH_FILE_NOT_EXIST).arg("topic"));
		return;
	}

	//Set window title
	this->setWindowTitle(WINDOW_TITLE.arg("Topic Graph - " + path));

	//Open a graph
	QSharedPointer<TopicGraph> topic_graph(new TopicGraph(path));
	//Open failed, prompt message and abort
	if (!topic_graph->loaded)
	{
		QMessageBox::information(NULL, "Error", FILE_OPEN_ERROR + topic_graph->failed_reason);
		return;
	}
	
	//Create new UI
	QWidget* new_central_widget = new QWidget(this);
	TopicGraphicsView* topic_view = new TopicGraphicsView(topic_graph);
	TopicPanel* topic_panel = new TopicPanel(topic_graph);
	//Set component size
	topic_view->setMinimumWidth(600);

	//PCA graph layout
	QHBoxLayout* layout = new QHBoxLayout();
	//Add components to layout
	layout->addWidget(topic_view);
	layout->addWidget(topic_panel);
	//Set layout strech and margin
	layout->setMargin(0);
	layout->setStretch(0, 3);
	layout->setStretch(1, 1);
	//Set layout
	new_central_widget->setLayout(layout);

	//Replace UI
	QWidget* old_central_widget = ui.centralWidget;
	this->setCentralWidget(new_central_widget);
	old_central_widget->setParent(NULL);
	delete old_central_widget;
	ui.centralWidget = new_central_widget;
	
	//Connect panel and view signals and slots
	//Node selected
	connect(topic_view, SIGNAL(nodeSelected(unsigned long long)), topic_panel, SLOT(showNodeInfo(unsigned long long)));
	//Edge selected
	connect(topic_view, SIGNAL(edgeSelected(unsigned long long)), topic_panel, SLOT(showEdgeInfo(unsigned long long)));
	//Scale changed
	connect(topic_view, SIGNAL(scaleChanged(double)), topic_panel, SLOT(scaleChangeHandler(double)));
	connect(topic_panel, SIGNAL(scaleChanged(double)), topic_view, SLOT(scaleChangeHandler(double)));
	//Layout changed
	connect(topic_panel, SIGNAL(layoutChanged(QString, bool)), topic_view, SLOT(applyLayout(QString, bool)));
	//Highlight node
	connect(topic_panel, SIGNAL(highlightNode(unsigned long long)), topic_view, SLOT(highlightNode(unsigned long long)));
}