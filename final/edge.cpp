#include "graph.h"

//Abstract edge class
//Constructor
Edge::Edge()
{
	this->graph = 0;
	this->node_a = 0;
	this->node_b = 0;
}

//Get graph reference
Graph* Edge::getGraph()
{
	return this->graph;
}

//Get connected nodes
//(Node A, source node)
Node* Edge::getNodeA()
{
	return this->node_a;
}

//(Node B, destination node)
Node* Edge::getNodeB()
{
	return this->node_b;
}

//Get effective edge (For non-directed graph)
Edge* Edge::getEffectiveEdge()
{
	auto effective_edge = this->prop<unsigned long long>("effective_edge");

	if (effective_edge == NULL)
		return this;
	else
		return reinterpret_cast<Edge*>(effective_edge);
}