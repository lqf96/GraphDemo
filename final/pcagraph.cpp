#include "pcagraph.h"

//PCA graph class
//Data parsers
QList<std::function<bool(QString, QVariant*)>> PCAGraph::data_parsers;

//Constructor
PCAGraph::PCAGraph(QString path)
{
	QFile nodes_file(path+"/Nodes.txt"), edges_file(path+"/Edges.txt");
	if ((!nodes_file.exists()) || (!edges_file.exists()))
	{
		this->loaded = false;
		this->failed_reason = "Corresponding file not found.";
		return;
	}

	if (!nodes_file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		this->loaded = false;
		this->failed_reason = "Can not open nodes record file.";
		return;
	}

	//Add node ID to property name table
	this->prop_table.append("node_id");

	QTextStream nodes_in(&nodes_file);
	QString line = nodes_in.readLine();
	while ((!nodes_in.atEnd()) && (line != ""))
	{
		Node* node = new Node();
		QRegExp line_regexp("^([^:]*): (.*)$");
		bool success;

		node->__prop__["node_id"] = line.toUInt(&success);
		if (!success)
		{
			this->loaded = false;
			this->failed_reason = "The nodes record file is broken.";
			delete node;
			return;
		}

		line = nodes_in.readLine();
		while (line != "")
		{
			int pos;
			QString prop_name, prop_data;
			
			pos = line_regexp.indexIn(line);
			if (pos == -1)
			{
				this->loaded = false;
				this->failed_reason = "The nodes record file is broken.";
				delete node;
				return;
			}

			prop_name = line_regexp.cap(1);
			prop_data = line_regexp.cap(2);

			if (!this->prop_table.contains(prop_name))
				this->prop_table.append(prop_name);

			for (auto data_parser : PCAGraph::data_parsers)
			{
				QVariant data;
				bool _success = data_parser(prop_data, &data);

				if (_success)
				{
					node->__prop__["ni_" + prop_name] = data;
					break;
				}
			}

			line = nodes_in.readLine();
		}

		this->addNode(node);
		line = nodes_in.readLine();
	}
	nodes_file.close();

	if (!edges_file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		this->loaded = false;
		this->failed_reason = "Can not open edges record file.";
		return;
	}

	QTextStream edges_in(&edges_file);
	line = edges_in.readLine();
	while ((!edges_in.atEnd()) && (line != ""))
	{
		unsigned int i = 0;
		unsigned int source_id, dest_id;
		Node* source_node;
		Node* dest_node;
		QRegExp line_regexp("^(\\d+) (\\d+)$");
		int pos;

		pos = line_regexp.indexIn(line);
		if (pos == -1)
		{
			this->loaded = false;
			this->failed_reason = "The edges record file is broken.";
			return;
		}

		source_id = line_regexp.cap(1).toUInt();
		dest_id = line_regexp.cap(2).toUInt();
		for (Node* node : this->getNodes())
		{
			if (node->__prop__["node_id"] == source_id)
				source_node = node;
			else if (node->__prop__["node_id"] == dest_id)
				dest_node = node;
		}

		source_node->connect(dest_node);
		line = edges_in.readLine();
	}
	edges_file.close();

	this->loaded = true;
}

//PCA graph data parser initialization
void PCAGraph::initDataParsers()
{
	//Add data parsers
	//Number list
	PCAGraph::data_parsers.append([](QString raw_data, QVariant* result)
	{
		if ((raw_data[0] != '(') || (raw_data[raw_data.size() - 1] != ')'))
			return false;
		
		QStringList raw_data_list = raw_data.mid(1, raw_data.size() - 2).split(',');
		QList<double> result_list;

		for (auto str : raw_data_list)
		{
			bool success;
			double converted_str = str.toDouble(&success);

			if (!success)
				return false;
			else
				result_list.append(converted_str);
		}

		result->setValue<QList<double>>(result_list);
		return true;
	});
	//Number
	PCAGraph::data_parsers.append([](QString raw_data, QVariant* result)
	{
		bool success;
		double _result = raw_data.toDouble(&success);

		if (success)
			*result = _result;
		return success;
	});
	//String (Dummy, works as fallback)
	PCAGraph::data_parsers.append([](QString raw_data, QVariant* result)
	{
		*result = raw_data.replace("\\n", "\n");
		return true;
	});
}