#pragma once

#include <cmath>
#include <ctime>
#include <qmainwindow.h>
#include <qpalette.h>
#include <qcolor.h>
#include <qfont.h>
#include <qlayout.h>
#include <qgridlayout.h>
#include <qlabel.h>
#include <qstring.h>
#include <qwidget.h>
#include <qrect.h>
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <qfile.h>
#include <qpoint.h>
#include "bsui.h"
#include "graph.h"
#include "pcagraph.h"
#include "topicgraph.h"
#include "gdlayout.h"
#include "gdgraphicsview.h"
#include "gdpanel.h"
#include "ui_graphdemo.h"

//Graph demo main window
class GraphDemo : public QMainWindow
{
	Q_OBJECT
public:
	//Constructor
	GraphDemo(QWidget *parent = 0);
	//Destructor
	~GraphDemo();

	//Slots
	public slots:
		//Show about
		void showAbout();
		//Open PCA / topic graph
		void openPCAGraph();
		void openTopicGraph();
private:
	//UI object
	Ui::GraphDemoClass ui;
};