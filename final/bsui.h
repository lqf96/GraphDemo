#pragma once

#include <functional>
#include <qhash.h>
#include <qbitmap.h>
#include <qpushbutton.h>
#include <qcolor.h>
#include <qevent.h>
#include <qstring.h>
#include <qwidget.h>
#include <qpainterpath.h>
#include <qpolygon.h>
#include <qrect.h>
#include <qfont.h>
#include <qmenu.h>
#include <qpalette.h>
#include <qaction.h>

//Bootstrap UI namespace
namespace BSUI
{
	//* Constants
	//Default font
	extern const QString DEFAULT_FONT;

	//* Helper functions
	//Color string helper
	inline QString rgbStr(QColor color)
	{
		return "rgb(" + QString::number(color.red()) + "," + QString::number(color.green()) + "," + QString::number(color.blue()) + ")";
	}
	//Font helper
	inline QFont BSUIFont(unsigned int size, unsigned int weight = QFont::Normal)
	{
		QFont font;
		font.setFamily(DEFAULT_FONT);
		font.setPixelSize(size);
		font.setWeight(weight);
		return font;
	}
	//Font color helper
	inline QPalette BSUIFontColor()
	{
		QPalette p;
		p.setColor(QPalette::WindowText, QColor(48, 48, 48));
		return p;
	}

	//* Classes
	//Abstract bootstrap UI component
	class BSUIComponent
	{private:
		//Event list and event handler binder
		QHash<QString, std::function<void()>> event_list;
		//Event handlers
		QHash<QString, std::function<void(void*)>> event_handlers;
	protected:
		//Register event
		void registerEvent(QString name, std::function<void()> binder);
		//Get event handler
		std::function<void(void*)> getEventHandler(QString name);
	public:
		//Add event listener
		void addEventListener(QString name, std::function<void(void*)> handler);
	};

	//BSUI Button
	class BSUIButton : public QPushButton, public BSUIComponent
	{
		Q_OBJECT
	public:
		//Button color struture
		struct ButtonStyle
		{
			//Color (Normal, hover, clicked)
			QColor normal_color;
			QColor hover_color;
			QColor clicked_color;
			//Border color (Normal, hover, clicked)
			QColor normal_border_color;
			QColor hover_border_color;
			QColor clicked_border_color;
			//Round rect radius
			unsigned int radius;
			//Font size
			unsigned int font_size;
			//Vertical padding
			unsigned int v_padding;
			//Horizontal padding
			unsigned int h_padding;
		};

		//Constructor
		BSUIButton(QString style_name, QString text = "", QBitmap* glyphicon = 0, QWidget* parent = 0);

		//Add a new button style
		static void addButtonStyle(QString name, ButtonStyle style);
	protected slots:
		//Click event slot
		void clickEventWrapper();
	private:
		//Button style
		static QHash<QString, ButtonStyle> styles;
	};
	//BSUI button initialization
	void BSUIButtonInit();

	//* Assistant functions
	//BSUI initialization function
	void BSUIInit();
}