#pragma once

#include <functional>
#include <qvariant.h>
#include <qfile.h>
#include <qhash.h>
#include <qregexp.h>
#include <qtextstream.h>
#include <qlist.h>
#include "graph.h"

//Topic graph class
class TopicGraph : public Graph
{public:
	//Document structure
	struct Document
	{
		QString title;
		QString content;
	};

	//Graph loaded or not
	bool loaded;
	//Failure reason
	QString failed_reason;

	//Documents
	QHash<unsigned int, Document> documents;

	//Constructor
	TopicGraph(QString path);
};