#include "gdlayout.h"

//PCA default layout strategy class
//Constructor
FixedPointLayoutStrategy::FixedPointLayoutStrategy(QSharedPointer<Graph> graph, unsigned int _fps, double _stable_time)
	: GraphLayoutStrategy(graph)
{
	using namespace std::placeholders;

	this->fps = _fps;
	this->stable_time = _stable_time;
	this->stable = false;

	//Create a new layout transition state
	this->newState(std::bind(RandomFinalState, _2, _3, _4, 300));
}

//Reset and establish layout animation state
void FixedPointLayoutStrategy::newState(std::function<void(Node*, double*, double*, double*)> func)
{
	QList<Node*> nodes = this->getGraph()->getNodes();

	this->counter = 0;
	this->stable = false;

	for (auto node : nodes)
	{
		double tx, ty, tz;
		TransitionInfo info;

		func(node, &tx, &ty, &tz);
		info.ax = 2 * (tx - node->prop<double>("x")) / (this->stable_time * this->stable_time);
		info.ay = 2 * (ty - node->prop<double>("y")) / (this->stable_time * this->stable_time);
		info.az = 2 * (tz - node->prop<double>("z")) / (this->stable_time * this->stable_time);

		layout_info[node] = info;
	}
}

//Run layout strategy
void FixedPointLayoutStrategy::run()
{
	if (this->stable)
		return;

	unsigned int total_frame = std::floor(this->fps * this->stable_time);
	double dt = 1 / (double)this->fps;
	double t = this->counter / (double)this->fps;
	auto state_ptr = this->layout_info.begin();
	while (state_ptr != this->layout_info.end())
	{
		Node* node = state_ptr.key();

		node->prop("x", node->prop<double>("x") + 0.5 * state_ptr->ax * (2 * this->stable_time - 2 * t - dt) * dt);
		node->prop("y", node->prop<double>("y") + 0.5 * state_ptr->ay * (2 * this->stable_time - 2 * t - dt) * dt);
		node->prop("z", node->prop<double>("z") + 0.5 * state_ptr->az * (2 * this->stable_time - 2 * t - dt) * dt);

		state_ptr++;
	}

	this->counter++;
	if (this->counter == total_frame)
		this->stable = true;
}

//Graph is stable or not
bool FixedPointLayoutStrategy::isStable()
{
	return this->stable;
}

//Fixed point layout final state function
//PCA default layout
void PCADefaultFinalState(Node* node, double* tx, double* ty, double* tz)
{
	QList<double> default_coordinate = node->prop<QList<double>>("ni_viewLayout");
	*tx = default_coordinate[0];
	*ty = default_coordinate[1];
	*tz = default_coordinate[2];
}

//Random layout strategy
void RandomFinalState(double* tx, double* ty, double* tz, int range)
{
	*tx = std::rand() % (range * 2);
	*ty = std::rand() % (range * 2);
	*tz = std::rand() % (range * 2);
}

//PCA Multi-level circular layout strategy constructor
PCAMultiLevelCircularFinalState::PCAMultiLevelCircularFinalState(QSharedPointer<PCAGraph> graph, double r)
{
	unsigned int p_amount = 0, c_amount = 0, a_amount = 0;

	for (auto node : graph->getNodes())
		if (node->__prop__["ni_type"] == "paper")
			p_amount++;
		else if (node->__prop__["ni_type"] == "conference")
			c_amount++;
		else if (node->__prop__["ni_type"] == "author")
			a_amount++;

	this->p_angle = 2 * PI / p_amount;
	this->c_angle = 2 * PI / c_amount;
	this->a_angle = 2 * PI / a_amount;

	this->p_count = 0;
	this->c_count = 0;
	this->a_count = 0;

	this->radius = r;
}

//PCA Multi-level circular layout strategy function call entry
void PCAMultiLevelCircularFinalState::operator ()(Node* node, double* tx, double* ty, double* tz)
{
	if (node->__prop__["ni_type"] == "paper")
	{
		*tx = (std::sin(this->p_count * this->p_angle) + 1) * 3 * this->radius;
		*ty = (std::cos(this->p_count * this->p_angle) + 1) * 3 * this->radius;
		*tz = 0;
		this->p_count++;
	}
	else if (node->__prop__["ni_type"] == "conference")
	{
		*tx = (std::sin(this->c_count * this->c_angle) + 3) * this->radius;
		*ty = (std::cos(this->c_count * this->c_angle) + 3) * this->radius;
		*tz = 0;
		this->c_count++;
	}
	else if (node->__prop__["ni_type"] == "author")
	{
		*tx = (std::sin(this->a_count * this->a_angle) * 2 + 3) * this->radius;
		*ty = (std::cos(this->a_count * this->a_angle) * 2 + 3) * this->radius;
		*tz = 0;
		this->a_count++;
	}
}

//Circular layout strategy constructor
CircularFinalState::CircularFinalState(QSharedPointer<Graph> graph, double r)
{
	this->count = 0;
	this->radius = r;
	this->angle = 2 * PI / (graph->getNodes().size());
}

//Circular layout strategy function call entry
//Function call entry
void CircularFinalState::operator()(double* tx, double* ty, double* tz)
{
	*tx = (std::sin(this->count * this->angle) + 1) * radius;
	*ty = (std::cos(this->count * this->angle) + 1) * radius;
	*tz = 0;

	this->count++;
}