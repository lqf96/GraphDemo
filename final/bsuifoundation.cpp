#include "bsui.h"

//* Abstract bootstrap UI component class
//Register event
void BSUI::BSUIComponent::registerEvent(QString name, std::function<void()> binder)
{
	this->event_list[name] = binder;
}

//Get event handler
std::function<void(void*)> BSUI::BSUIComponent::getEventHandler(QString name)
{
	if (this->event_handlers.contains(name))
		return this->event_handlers[name];
	else
		throw QString("EventHandlerNotExist");
}

//Add event listener
void BSUI::BSUIComponent::addEventListener(QString name, std::function<void(void*)> handler)
{
	if (this->event_list.contains(name))
	{
		this->event_list[name]();
		this->event_handlers[name] = handler;
	}
	else
		throw "EventNotExist";
}

//* Assistant functions
//BSUI initialization function
void BSUI::BSUIInit()
{
	//BSUI button
	BSUI::BSUIButtonInit();
}

//* Constants
//Default font
const QString BSUI::DEFAULT_FONT = "\"Helvetica Neue\", Helvetica, Arial, sans-serif";