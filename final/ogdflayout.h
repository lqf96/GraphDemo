#pragma once

#include <qhash.h>
#include <qsharedpointer.h>
#include <ogdf/energybased/FMMMLayout.h>
#include <ogdf/energybased/TutteLayout.h>
#include "graph.h"

//Constants
//Layout scale factor
const double TOPIC_FMMM_SCALE_FACTOR = 0.5;
const double TOPIC_TUTTE_SCALE_FACTOR = 0.333333;
const double PCA_FMMM_SCALE_FACTOR = 2.5;
const double PCA_TUTTE_SCALE_FACTOR = 0.333333;

//OGDF layout final state
class OGDFLayoutFinalState
{
	//OGDF graph and graph attribute
	ogdf::Graph ogdf_graph;
	ogdf::GraphAttributes ogdf_ga;
	//GD node / OGDF node convertion table
	QHash<Node*, ogdf::node> node_table;

	//Scale factor
	double scale_factor;
public:
	//OGDF layout flag
	enum OGDFLayout
	{
		FMMMLayout = 0,
		TutteLayout
	};

	//Constructor
	OGDFLayoutFinalState(QSharedPointer<Graph> _graph, OGDFLayout ogdf_layout, double _scale_factor = 1);
	//Function call entry
	void operator ()(Node* node, double* dx, double* dy, double* dz);
};