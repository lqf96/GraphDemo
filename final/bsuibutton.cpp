#include "bsui.h"

//BSUI button class
//Button style
QHash<QString, BSUI::BSUIButton::ButtonStyle> BSUI::BSUIButton::styles;

//Constructor
BSUI::BSUIButton::BSUIButton(QString style_name, QString text, QBitmap* glyphicon, QWidget* parent) : QPushButton(text, parent)
{
	//Install event filter
	this->installEventFilter(this);
	//Register click event
	this->registerEvent("click", [this]()
	{
		connect(this, SIGNAL(clicked()), this, SLOT(clickEventWrapper()));
	});

	//Get button style
	ButtonStyle style;
	if (BSUIButton::styles.contains(style_name))
		style = BSUIButton::styles[style_name];
	else
		throw QString("ButtonStyleNotExist");

	//Set button style
	this->setStyleSheet(
		//Normal
		"QPushButton"
		"{   background-color: " + BSUI::rgbStr(style.normal_color) + ";"
		"    border-color: " + BSUI::rgbStr(style.normal_border_color) + ";"
		"    color: #fff;"
		"    padding-top: " + QString::number(style.v_padding) + ";"
		"    padding-bottom: " + QString::number(style.v_padding) + ";"
		"    padding-left: " + QString::number(style.h_padding) + ";"
		"    padding-right: " + QString::number(style.h_padding) + ";"
		"    border-radius: " + QString::number(style.radius) + "px;"
		"    border: 1px solid transparent;"
		"}"
		//Hover
		"QPushButton:hover"
		"{   background-color: " + BSUI::rgbStr(style.hover_color) + ";"
		"    border-color: " + BSUI::rgbStr(style.hover_border_color) + ";"
		"}"
		//Clicked (Pressed)
		"QPushButton:pressed"
		"{   background-color: " + BSUI::rgbStr(style.clicked_color) + ";"
		"    border-color: " + BSUI::rgbStr(style.clicked_border_color) + ";"
		"}");
	//Set font
	QFont btn_font = this->font();
	btn_font.setPixelSize(style.font_size);
	btn_font.setFamily(BSUI::DEFAULT_FONT);
	this->setFont(btn_font);
}

//Click event handler wrapper
void BSUI::BSUIButton::clickEventWrapper()
{
	this->getEventHandler("click")(NULL);
}

//Add a new button style
void BSUI::BSUIButton::addButtonStyle(QString name, ButtonStyle style)
{
	BSUIButton::styles[name] = style;
}

//BSUI button initialization
void BSUI::BSUIButtonInit()
{
	//BSUI button style initialization
	//.btn-primary .btn-lg
	BSUI::BSUIButton::addButtonStyle("btn-primary btn-lg",
	{	//Background colors
		QColor(51, 122, 183), QColor(40, 96, 144), QColor(32, 77, 116),
		//Border colors
		QColor(46, 109, 164), QColor(32, 77, 116), QColor(25, 61, 94),
		//Size
		6, 18, 10, 16
	});
	//.btn-info .btn-lg
	BSUI::BSUIButton::addButtonStyle("btn-info btn-lg",
	{	//Background colors
		QColor(91, 192, 222), QColor(49, 176, 213), QColor(38, 154, 188),
		//Border colors
		QColor(70, 184, 218), QColor(38, 154, 188), QColor(34, 138, 169),
		//Size
		6, 18, 10, 16
	});
	//.btn-warning .btn-lg
	BSUI::BSUIButton::addButtonStyle("btn-warning btn-lg",
	{	//Background colors
		QColor(240, 173, 78), QColor(236, 151, 31), QColor(213, 133, 18),
		//Border colors
		QColor(238, 162, 54), QColor(213, 133, 18), QColor(161, 99, 12),
		//Size
		6, 18, 10, 16
	});
}