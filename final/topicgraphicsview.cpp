#include "gdgraphicsview.h"

//* Constants
//Node color endpoint
const QColor NODE_COLOR_A(Qt::GlobalColor::blue);
const QColor NODE_COLOR_B(Qt::GlobalColor::green);
//Random point range
const unsigned int RND_RANGE = 300;
//Selected range border width
const double SELECTED_RANGE_WIDTH = 0.5;
//Free drag reset probability
const unsigned int FREE_DRAG_FACTOR = 5;

//Topic graphics node item class
//Constructor
TopicGraphicsNodeItem::TopicGraphicsNodeItem(TopicGraphicsView* _self, Node* _node, unsigned int max_node_conn)
{
	//Initialization
	this->node = _node;
	this->self = _self;
	this->mouse_status = MouseState::NotPressed;

	//Generate random point position
	double x, y, z;
	RandomFinalState(&x, &y, &z, RND_RANGE);
	_node->__prop__["x"] = x;
	_node->__prop__["y"] = y;
	_node->__prop__["z"] = z;

	//Calculate color
	QColor node_color;
	unsigned int conn_node_count = node->getConnectedEdges().size();
	double ratio = 2 - 2 / ((double)conn_node_count / max_node_conn + 1);
	node_color.setRedF(NODE_COLOR_A.redF() * (1 - ratio) + NODE_COLOR_B.redF() * ratio);
	node_color.setGreenF(NODE_COLOR_A.greenF() * (1 - ratio) + NODE_COLOR_B.greenF() * ratio);
	node_color.setBlueF(NODE_COLOR_A.blueF() * (1 - ratio) + NODE_COLOR_B.blueF() * ratio);
	//Create bounding rectangle
	QRectF node_item_rect(0, 0, 2 * NODE_ITEM_RADIUS, 2 * NODE_ITEM_RADIUS);
	//Set bounding rect and color
	this->setBrush(node_color);
	this->setPen(node_color);
	this->setRect(node_item_rect);

	//Set tooltip text
	this->setToolTip("Node #" + QString::number(this->node->prop<unsigned int>("topic_id")));

	//Set Z value to make node on top of the edge
	this->setZValue(1);

	//Allow event handling
	this->setAcceptedMouseButtons(Qt::LeftButton);
	this->setAcceptHoverEvents(true);
	//Allow selecting
	this->setFlag(QGraphicsItem::ItemIsSelectable, true);

	//Add node graphics item reference and node color to node
	node->prop("__gitem", this);
	node->prop("__gcolor", node_color);

	//Set highlight handler and unset handler
	QVariant highlight_handler;
	QVariant unset_handler;

	highlight_handler.setValue<std::function<void()>>([this]()
	{
		this->setBrush(Qt::GlobalColor::white);
		this->setPen(QColor(Qt::GlobalColor::white));
	});
	unset_handler.setValue<std::function<void()>>([this]()
	{
		QColor node_color = this->node->prop<QColor>("__gcolor");
		this->setBrush(node_color);
		this->setPen(node_color);
	});

	this->setData(HIGHLIGHT_HANDLER, highlight_handler);
	this->setData(UNSET_HIGHLIGHT_HANDLER, unset_handler);
}

//Hover enter event
void TopicGraphicsNodeItem::hoverEnterEvent(QGraphicsSceneHoverEvent* e)
{
	(this->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->setCursor(Qt::PointingHandCursor);
}

//Hover leave event
void TopicGraphicsNodeItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* e)
{
	if (!this->isSelected())
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->unsetCursor();
}

//Mouse pressed event
void TopicGraphicsNodeItem::mousePressEvent(QGraphicsSceneMouseEvent* e)
{
	self->nodeSelectedWrapper(reinterpret_cast<unsigned long long>(this->node));
	this->mouse_status = MouseState::Pressed;

	//Current item not contained in selected items, deselect them
	auto selected_items = self->topic_scene->selectedItems();
	if (!selected_items.contains(this))
	{
		for (auto item : selected_items)
			item->setSelected(false);
		this->setSelected(true);
	}
}

//Mouse move event
void TopicGraphicsNodeItem::mouseMoveEvent(QGraphicsSceneMouseEvent* e)
{
	if (this->mouse_status == MouseState::NotPressed)
		return;

	double dx = e->scenePos().x() - e->lastScenePos().x(),
		dy = e->scenePos().y() - e->lastScenePos().y();
	for (auto item : self->topic_scene->selectedItems())
		((TopicGraphicsNodeItem*)item)->moveNode(dx, dy);

	//Free drag mode (Random triggered)
	if (self->allow_free_drag)
		if (std::rand() % FREE_DRAG_FACTOR == 0)
			self->state_reset_handler(self->layout);

	if (this->mouse_status == MouseState::Pressed)
	{
		this->setCursor(Qt::ClosedHandCursor);
		this->mouse_status = MouseState::Moved;
	}
}

//Mouse release event
void TopicGraphicsNodeItem::mouseReleaseEvent(QGraphicsSceneMouseEvent * e)
{
	this->setCursor(Qt::PointingHandCursor);
	this->mouse_status = MouseState::NotPressed;
}

//Move node
//(With dx and dy)
void TopicGraphicsNodeItem::moveNode(double dx, double dy)
{
	this->moveBy(dx, dy);
	this->node->prop("x", this->node->prop<double>("x") + dx);
	this->node->prop("y", this->node->prop<double>("y") + dy);

	for (auto edge : this->node->getConnectedEdges())
	{
		auto edge_item = edge->getEffectiveEdge()->prop<TopicGraphicsEdgeItem*>("__gitem");
		edge_item->moveEdge();
	}
}

//(Without dx and dy)
void TopicGraphicsNodeItem::moveNode()
{
	this->setPos(this->node->prop<double>("x") - NODE_ITEM_RADIUS,
		this->node->prop<double>("y") - NODE_ITEM_RADIUS);

	for (auto edge : this->node->getConnectedEdges())
	{
		auto edge_item = edge->getEffectiveEdge()->prop<TopicGraphicsEdgeItem*>("__gitem");
		edge_item->moveEdge();
	}
}

//Graphics item change
QVariant TopicGraphicsNodeItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
	//Unset highlight when the item is unselected
	if ((change == QGraphicsItem::ItemSelectedHasChanged) && (!this->isSelected()))
	{
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
		this->setData(__MULTI_SELECTED, false);

		return true;
	}
	//Other changes are handled by base class handler
	else
		return QGraphicsEllipseItem::itemChange(change, value);
}

//Topic graphics edge item class
//Constructor
TopicGraphicsEdgeItem::TopicGraphicsEdgeItem(TopicGraphicsView* _self, Edge* _edge)
{
	this->edge = _edge;
	this->self = _self;

	double x1 = _edge->getNodeA()->prop<double>("x"),
		y1 = _edge->getNodeA()->prop<double>("y"),
		x2 = _edge->getNodeB()->prop<double>("x"),
		y2 = _edge->getNodeB()->prop<double>("y");

	//Set linear gradient
	QLinearGradient gradient(x1, y1, x2, y2);
	gradient.setColorAt(0, _edge->getNodeA()->prop<QColor>("__gcolor"));
	gradient.setColorAt(1, _edge->getNodeB()->prop<QColor>("__gcolor"));
	//Create QPen object and apply QPen
	//(Use edge weight as width)
	this->_pen = QPen(gradient, _edge->prop<double>("weight"));
	this->setPen(this->_pen);
	//Set line
	this->setLine(x1, y1, x2, y2);

	//Set tooltip text
	this->setToolTip("Edge");

	//Allow event handling
	this->setAcceptedMouseButtons(Qt::LeftButton);
	this->setAcceptHoverEvents(true);
	//Allow selecting
	this->setFlag(QGraphicsItem::ItemIsSelectable, true);
	//Single selection flag set to false by default
	this->single_flag = false;

	//Add edge graphics item reference to edge
	this->edge->prop("__gitem", this);

	//Set highlight handler and unset handler
	QVariant highlight_handler;
	QVariant unset_handler;
	
	highlight_handler.setValue<std::function<void()>>([this]()
	{
		this->setPen(QColor(Qt::GlobalColor::white));
	});
	unset_handler.setValue<std::function<void()>>([this]()
	{
		this->setPen(this->_pen);
	});

	this->setData(HIGHLIGHT_HANDLER, highlight_handler);
	this->setData(UNSET_HIGHLIGHT_HANDLER, unset_handler);
}

//Hover enter event
void TopicGraphicsEdgeItem::hoverEnterEvent(QGraphicsSceneHoverEvent* e)
{
	(this->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->setCursor(Qt::PointingHandCursor);
}

//Hover leave event
void TopicGraphicsEdgeItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* e)
{
	if (!this->isSelected())
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	this->unsetCursor();
}

//Mouse pressed event
void TopicGraphicsEdgeItem::mousePressEvent(QGraphicsSceneMouseEvent* e)
{
	//Emit edge selected signal
	self->edgeSelectedWrapper(reinterpret_cast<unsigned long long>(this->edge));

	//Unselect other selected items
	for (auto item : self->topic_scene->selectedItems())
		item->setSelected(false);
	//Select current item
	this->single_flag = true;
	this->setSelected(true);
}

//Move edge
void TopicGraphicsEdgeItem::moveEdge()
{
	//Get endpoint coordinate
	double x1 = this->edge->getNodeA()->prop<double>("x"),
		y1 = this->edge->getNodeA()->prop<double>("y"),
		x2 = this->edge->getNodeB()->prop<double>("x"),
		y2 = this->edge->getNodeB()->prop<double>("y");
	//Redraw and set line
	QLine new_line(x1, y1, x2, y2);
	this->setLine(new_line);

	//Regenerate gradient effect
	QLinearGradient gradient(x1, y1, x2, y2);
	gradient.setColorAt(0, this->edge->getNodeA()->prop<QColor>("__gcolor"));
	gradient.setColorAt(1, this->edge->getNodeB()->prop<QColor>("__gcolor"));
	//Set pen
	this->setPen(QPen(gradient, this->edge->prop<double>("weight")));
}

//Graphics item change
QVariant TopicGraphicsEdgeItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
	//Item is being selected
	if ((change == QGraphicsItem::ItemSelectedChange) && value.toBool())
	{
		//Single selection state
		if (this->single_flag)
		{
			this->single_flag = false;
			return true;
		}
		//Multiple selection state, blocked
		else
			return false;
	}
	//Unset highlight when the item is unselected
	else if ((change == QGraphicsItem::ItemSelectedHasChanged) && (!this->isSelected()))
	{
		(this->data(UNSET_HIGHLIGHT_HANDLER).value<std::function<void()>>())();
		this->setData(__MULTI_SELECTED, false);

		return true;
	}
	//Other changes are handled by base class handler
	else
		return QGraphicsLineItem::itemChange(change, value);
}

//Topic graphics view class
//Constructor
TopicGraphicsView::TopicGraphicsView(QSharedPointer<TopicGraph> _graph, QWidget* parent) : QGraphicsView(parent)
{
	this->graph = _graph;

	//Initialize layout transition timer
	this->layout_timer = new QTimer(this);
	this->layout_timer->setInterval(1000 / FPS);
	connect(layout_timer, SIGNAL(timeout()), this, SLOT(layoutTransitionHandler()));

	//Initialize layout reference
	this->layout = NULL;

	//Init 2D environment
	this->init2D();
}

//2D environment initialization
void TopicGraphicsView::init2D()
{
	unsigned int max_node_conn = 0;
	QList<Node*> nodes = this->graph->getNodes();

	//Set up scene
	this->topic_scene = new QGraphicsScene(this);
	this->setScene(topic_scene);
	//Set background
	topic_scene->setBackgroundBrush(Qt::GlobalColor::black);

	//Set scale status
	this->scale_status = 0;
	this->scale_at_view_side = false;

	//Set drag mode
	this->setDragMode(QGraphicsView::ScrollHandDrag);

	for (auto node : nodes)
	{
		unsigned int conn_count = node->getConnectedEdges().size();
		if (conn_count > max_node_conn)
			max_node_conn = conn_count;
	}

	//Create topic graphics node item and add to scene
	for (auto node : nodes)
	{
		auto node_item = new TopicGraphicsNodeItem(this, node, max_node_conn);
		topic_scene->addItem(node_item);

		//Set node graphics item position
		//(Cannot be done in node item constructor as this will cause coordinate problem)
		node_item->setPos(node->prop<double>("x"), node->prop<double>("y"));
	}

	//Create topic graphics edge item and add to scene
	for (auto edge : this->graph->getEdges())
	{
		auto edge_item = new TopicGraphicsEdgeItem(this, edge);
		topic_scene->addItem(edge_item);
	}

	//Connect signals and slots
	connect(this, SIGNAL(rubberBandChanged(QRect, QPointF, QPointF)), this, SLOT(rubberBandChangeHandler(QRect, QPointF, QPointF)));

	//Disable free drag at beginning
	this->allow_free_drag = false;
}

//Mouse wheel event
void TopicGraphicsView::wheelEvent(QWheelEvent* e)
{
	double ds = e->delta() / SCALE_FACTOR;
	if ((this->scale_status + ds > UPPER_SCALE_RANGE) || (this->scale_status + ds < LOWER_SCALE_RANGE))
		return;
	this->scale(std::exp(ds), std::exp(ds));
	this->scale_status += ds;

	this->scale_at_view_side = true;
	emit scaleChanged(this->scale_status);
}

//Scale change handler
void TopicGraphicsView::scaleChangeHandler(double scale_value)
{
	if (this->scale_at_view_side)
	{
		this->scale_at_view_side = false;
		return;
	}

	this->scale(std::exp(scale_value - this->scale_status), std::exp(scale_value - this->scale_status));
	this->scale_status = scale_value;
}

//Apply layout
void TopicGraphicsView::applyLayout(QString _layout_name, bool _allow_free_drag)
{
	using namespace std::placeholders;

	GraphLayoutStrategy* new_layout;
	GraphLayoutStrategy* old_layout = this->layout;
	this->allow_free_drag = _allow_free_drag;

	//Random layout
	if (_layout_name == "Random")
	{
		FixedPointLayoutStrategy* random_layout = new FixedPointLayoutStrategy(this->graph);
		random_layout->newState(std::bind(RandomFinalState, _2, _3, _4, RND_RANGE));
		new_layout = random_layout;
	}
	//Circular layout
	else if (_layout_name == "Circular")
	{
		FixedPointLayoutStrategy* circular_layout = new FixedPointLayoutStrategy(this->graph);
		circular_layout->newState(std::bind(CircularFinalState(this->graph), _2, _3, _4));
		new_layout = circular_layout;
	}
	//OGDF FMMM layout
	else if (_layout_name == "OGDFFMMM")
	{
		FixedPointLayoutStrategy* ogdf_fmmm_layout = new FixedPointLayoutStrategy(this->graph);
		ogdf_fmmm_layout->newState(OGDFLayoutFinalState(this->graph, OGDFLayoutFinalState::OGDFLayout::FMMMLayout, TOPIC_FMMM_SCALE_FACTOR));
		new_layout = ogdf_fmmm_layout;
	}
	//OGDF tutte layout
	else if (_layout_name == "OGDFTutte")
	{
		FixedPointLayoutStrategy* ogdf_tutte_layout = new FixedPointLayoutStrategy(this->graph);
		ogdf_tutte_layout->newState(OGDFLayoutFinalState(this->graph, OGDFLayoutFinalState::OGDFLayout::TutteLayout, TOPIC_TUTTE_SCALE_FACTOR));
		new_layout = ogdf_tutte_layout;
	}
	//Elastic force directed layout
	else if (_layout_name == "ElasticForce")
	{
		auto elastic_force_layout = new ForceDirectedLayoutStrategy(this->graph);
		elastic_force_layout->newState(std::bind(ElasticForce, _1, _2, _3, _4, ELASTIC_STABLE_LENGTH, ELASTIC_K));
		new_layout = elastic_force_layout;

		//Free drag layout reset handler
		this->state_reset_handler = [](GraphLayoutStrategy* _layout)
		{
			auto layout = (ForceDirectedLayoutStrategy*)_layout;
			auto nfc = layout->getNodeForceFunc();
			auto efc = layout->getEdgeForceFunc();

			//Reset and start a new state
			layout->newState(efc, nfc);
		};
	}
	
	this->layout = new_layout;
	if (old_layout != NULL)
		delete old_layout;

	//Start timer
	this->layout_timer->start();
}

//Layout transition handler
void TopicGraphicsView::layoutTransitionHandler()
{
	this->layout->run();
	if (this->layout->isStable())
		this->layout_timer->stop();

	for (auto node : this->graph->getNodes())
	{
		auto node_item = node->prop<TopicGraphicsNodeItem*>("__gitem");
		node_item->moveNode();
	}
}

//Key press event
void TopicGraphicsView::keyPressEvent(QKeyEvent* e)
{
	//Ctrl pressed, enter rubber band mode
	if (e->key() == Qt::Key::Key_Control)
		this->setDragMode(DragMode::RubberBandDrag);
}

//Key release event
void TopicGraphicsView::keyReleaseEvent(QKeyEvent* e)
{
	//Ctrl pressed, exit rubber band mode
	if (e->key() == Qt::Key::Key_Control)
		this->setDragMode(DragMode::ScrollHandDrag);
}

//Rubber band change handler
void TopicGraphicsView::rubberBandChangeHandler(QRect, QPointF, QPointF)
{
	for (auto item : this->topic_scene->selectedItems())
		if (item->data(__MULTI_SELECTED) != true)
		{
			(item->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
			item->setData(__MULTI_SELECTED, true);
		}
}

//Highlight node
void TopicGraphicsView::highlightNode(unsigned long long _node)
{
	for (auto item : this->topic_scene->selectedItems())
		item->setSelected(false);
	
	auto node = reinterpret_cast<Node*>(_node);
	auto node_item = node->prop<TopicGraphicsNodeItem*>("__gitem");
	(node_item->data(HIGHLIGHT_HANDLER).value<std::function<void()>>())();
	node_item->setSelected(true);
}

//Qt signal wrapper
void TopicGraphicsView::nodeSelectedWrapper(unsigned long long _node){ emit nodeSelected(_node); }
void TopicGraphicsView::edgeSelectedWrapper(unsigned long long _edge){ emit edgeSelected(_edge); }

//Qt meta type registration
Q_DECLARE_METATYPE(TopicGraphicsNodeItem*)
Q_DECLARE_METATYPE(TopicGraphicsEdgeItem*)
Q_DECLARE_METATYPE(std::function<void()>)