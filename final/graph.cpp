#include "graph.h"

//Abstract graph class
//Constructor
Graph::Graph(bool _directed)
{
	this->directed = _directed;
}

//Destructor
Graph::~Graph()
{
	for (Node* node : this->nodes)
		delete node;
	for (auto node_edges : this->adj_matrix)
		for (Edge* edge : node_edges)
			if (edge != NULL)
				delete edge;
}

//Directed or non-directed edge
bool Graph::isDirected()
{
	return this->directed;
}

//Add node
void Graph::addNode(Node* node)
{
	if (node == 0)
		throw QString("IllegalPointer");
	if (node->graph != 0)
		throw QString("AlreadyAddedToGraph");

	QList<Edge*> new_node_edges;
	unsigned int i;

	node->graph = this;
	nodes.append(node);
	for (i = 0; i < this->adj_matrix.size();i++)
	{
		QList<Edge*> node_edges = this->adj_matrix[i];
		node_edges.append(NULL);
		this->adj_matrix[i] = node_edges;
		
		new_node_edges.append(NULL);
	}
	new_node_edges.append(NULL);
	this->adj_matrix.append(new_node_edges);
}

//Remove node
void Graph::removeNode(Node* node, bool release)
{
	if ((node == 0) || (node->graph != this))
		return;

	unsigned int i;
	for (i = 0; i < this->nodes.size(); i++)
		if (nodes[i] == node)
			break;

	this->nodes.removeAt(i);
	for (Edge* edge : this->adj_matrix[i])
		if (edge != NULL)
			delete edge;
	this->adj_matrix.removeAt(i);
	for (auto node_edges : this->adj_matrix)
	{
		if (node_edges[i] != NULL)
			delete node_edges[i];
		node_edges.removeAt(i);
	}

	if (release)
		delete node;
}

//Filter and get node collection (All nodes by default)
QList<Node*> Graph::getNodes(std::function<bool(Node*)> func)
{
	QList<Node*> result;
	for (Node* node : this->nodes)
		if (func(node))
			result.append(node);
	return result;
}

//Filter and get edge collection
//(All edges by default for directed graphs)
//(Non-duplicated result for non-directed graphs)
QList<Edge*> Graph::getEdges(std::function<bool(Edge*)> func, bool no_duplicated_edge)
{
	QList<Edge*> result;
	unsigned int i, j;

	for (i = 0; i < adj_matrix.size(); i++)
		if ((!this->directed) && no_duplicated_edge)
		{
			for (j = i + 1; j < adj_matrix[i].size(); j++)
				if ((adj_matrix[i][j] != NULL) && func(adj_matrix[i][j]))
					result.append(adj_matrix[i][j]);
		}
		else
			for (j = 0; j < adj_matrix[i].size(); j++)
				if ((adj_matrix[i][j] != NULL) && func(adj_matrix[i][j]))
					result.append(adj_matrix[i][j]);

	return result;
}

//Connect two nodes
Edge* Graph::connect(Node* node1, Node* node2, Edge** p_edge2)
{
	if ((node1 == NULL) || (node2 == NULL))
		throw QString("IllegalPointer");
	else if (node1 == node2)
		throw QString("SelfNotAllowedToAdd");
	else if (node1->graph != node2->graph)
		throw QString("NotSameGraph");
	else if (node1->graph != this)
		throw QString("NotThisGraph");

	unsigned int source_index, dest_index;
	unsigned int i;
	for (i = 0; i < this->nodes.size(); i++)
		if (node1 == this->nodes[i])
			source_index = i;
		else if (node2 == this->nodes[i])
			dest_index = i;

	auto edge1 = new Edge();
	edge1->graph = this;
	edge1->node_a = node1;
	edge1->node_b = node2;
	this->adj_matrix[source_index][dest_index] = edge1;

	if (!this->directed)
	{
		auto edge2 = new Edge();
		edge2->graph = this;
		edge2->node_a = node2;
		edge2->node_b = node1;
		this->adj_matrix[dest_index][source_index] = edge2;

		if (source_index < dest_index)
		{
			edge2->prop("effective_edge", reinterpret_cast<unsigned long long>(edge1));
			if (p_edge2 != NULL)
				*p_edge2 = edge2;
			return edge1;
		}
		else
		{
			edge1->prop("effective_edge", reinterpret_cast<unsigned long long>(edge2));
			if (p_edge2 != NULL)
				*p_edge2 = edge1;
			return edge2;
		}
	}
	else
		return edge1;
}

//Remove edge
void Graph::removeEdge(Edge* edge)
{
	if (edge == NULL)
		throw QString("IllegalPointer");
	else if (edge->graph != this)
		throw QString("NotThisGraph");

	unsigned int source_index, dest_index;
	unsigned int i;
	for (i = 0; i < this->nodes.size(); i++)
		if (edge->node_a == this->nodes[i])
			source_index = i;
		else if (edge->node_b == this->nodes[i])
			dest_index = i;

	delete adj_matrix[source_index][dest_index];
	adj_matrix[source_index][dest_index] = NULL;
	if (!this->directed)
	{
		delete adj_matrix[dest_index][source_index];
		adj_matrix[dest_index][source_index] = NULL;
	}
}