/********************************************************************************
** Form generated from reading UI file 'graphdemo.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRAPHDEMO_H
#define UI_GRAPHDEMO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GraphDemoClass
{
public:
    QAction *actionOpenPCAGraph;
    QAction *actionOpenTopicGraph;
    QAction *actionExit;
    QAction *actionAbout;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuAbout;

    void setupUi(QMainWindow *GraphDemoClass)
    {
        if (GraphDemoClass->objectName().isEmpty())
            GraphDemoClass->setObjectName(QStringLiteral("GraphDemoClass"));
        GraphDemoClass->resize(800, 600);
        GraphDemoClass->setMinimumSize(QSize(800, 600));
        GraphDemoClass->setAutoFillBackground(false);
        actionOpenPCAGraph = new QAction(GraphDemoClass);
        actionOpenPCAGraph->setObjectName(QStringLiteral("actionOpenPCAGraph"));
        actionOpenTopicGraph = new QAction(GraphDemoClass);
        actionOpenTopicGraph->setObjectName(QStringLiteral("actionOpenTopicGraph"));
        actionExit = new QAction(GraphDemoClass);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionAbout = new QAction(GraphDemoClass);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralWidget = new QWidget(GraphDemoClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        GraphDemoClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(GraphDemoClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 800, 23));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuAbout = new QMenu(menuBar);
        menuAbout->setObjectName(QStringLiteral("menuAbout"));
        GraphDemoClass->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuAbout->menuAction());
        menuFile->addAction(actionOpenPCAGraph);
        menuFile->addAction(actionOpenTopicGraph);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuAbout->addAction(actionAbout);

        retranslateUi(GraphDemoClass);
        QObject::connect(actionExit, SIGNAL(triggered()), GraphDemoClass, SLOT(close()));
        QObject::connect(actionAbout, SIGNAL(triggered()), GraphDemoClass, SLOT(showAbout()));
        QObject::connect(actionOpenPCAGraph, SIGNAL(triggered()), GraphDemoClass, SLOT(openPCAGraph()));
        QObject::connect(actionOpenTopicGraph, SIGNAL(triggered()), GraphDemoClass, SLOT(openTopicGraph()));

        QMetaObject::connectSlotsByName(GraphDemoClass);
    } // setupUi

    void retranslateUi(QMainWindow *GraphDemoClass)
    {
        GraphDemoClass->setWindowTitle(QApplication::translate("GraphDemoClass", "GraphDemo", 0));
        actionOpenPCAGraph->setText(QApplication::translate("GraphDemoClass", "Open PCA Graph", 0));
        actionOpenTopicGraph->setText(QApplication::translate("GraphDemoClass", "Open Topic Graph", 0));
        actionExit->setText(QApplication::translate("GraphDemoClass", "Exit", 0));
        actionAbout->setText(QApplication::translate("GraphDemoClass", "About", 0));
        menuFile->setTitle(QApplication::translate("GraphDemoClass", "File", 0));
        menuAbout->setTitle(QApplication::translate("GraphDemoClass", "Help", 0));
    } // retranslateUi

};

namespace Ui {
    class GraphDemoClass: public Ui_GraphDemoClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRAPHDEMO_H
