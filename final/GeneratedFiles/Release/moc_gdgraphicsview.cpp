/****************************************************************************
** Meta object code from reading C++ file 'gdgraphicsview.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../gdgraphicsview.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gdgraphicsview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PCAGraphicsView_t {
    QByteArrayData data[15];
    char stringdata0[203];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PCAGraphicsView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PCAGraphicsView_t qt_meta_stringdata_PCAGraphicsView = {
    {
QT_MOC_LITERAL(0, 0, 15), // "PCAGraphicsView"
QT_MOC_LITERAL(1, 16, 12), // "nodeSelected"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 5), // "_node"
QT_MOC_LITERAL(4, 36, 12), // "edgeSelected"
QT_MOC_LITERAL(5, 49, 5), // "_edge"
QT_MOC_LITERAL(6, 55, 12), // "scaleChanged"
QT_MOC_LITERAL(7, 68, 11), // "scale_value"
QT_MOC_LITERAL(8, 80, 18), // "scaleChangeHandler"
QT_MOC_LITERAL(9, 99, 11), // "applyLayout"
QT_MOC_LITERAL(10, 111, 12), // "_layout_name"
QT_MOC_LITERAL(11, 124, 16), // "_allow_free_drag"
QT_MOC_LITERAL(12, 141, 23), // "layoutTransitionHandler"
QT_MOC_LITERAL(13, 165, 23), // "rubberBandChangeHandler"
QT_MOC_LITERAL(14, 189, 13) // "highlightNode"

    },
    "PCAGraphicsView\0nodeSelected\0\0_node\0"
    "edgeSelected\0_edge\0scaleChanged\0"
    "scale_value\0scaleChangeHandler\0"
    "applyLayout\0_layout_name\0_allow_free_drag\0"
    "layoutTransitionHandler\0rubberBandChangeHandler\0"
    "highlightNode"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PCAGraphicsView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,
       6,    1,   60,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   63,    2, 0x0a /* Public */,
       9,    2,   66,    2, 0x0a /* Public */,
      12,    0,   71,    2, 0x0a /* Public */,
      13,    3,   72,    2, 0x0a /* Public */,
      14,    1,   79,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::ULongLong,    3,
    QMetaType::Void, QMetaType::ULongLong,    5,
    QMetaType::Void, QMetaType::Double,    7,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    7,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   10,   11,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QRect, QMetaType::QPointF, QMetaType::QPointF,    2,    2,    2,
    QMetaType::Void, QMetaType::ULongLong,    3,

       0        // eod
};

void PCAGraphicsView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PCAGraphicsView *_t = static_cast<PCAGraphicsView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->nodeSelected((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 1: _t->edgeSelected((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 2: _t->scaleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->scaleChangeHandler((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->applyLayout((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 5: _t->layoutTransitionHandler(); break;
        case 6: _t->rubberBandChangeHandler((*reinterpret_cast< QRect(*)>(_a[1])),(*reinterpret_cast< QPointF(*)>(_a[2])),(*reinterpret_cast< QPointF(*)>(_a[3]))); break;
        case 7: _t->highlightNode((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PCAGraphicsView::*_t)(unsigned long long );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PCAGraphicsView::nodeSelected)) {
                *result = 0;
            }
        }
        {
            typedef void (PCAGraphicsView::*_t)(unsigned long long );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PCAGraphicsView::edgeSelected)) {
                *result = 1;
            }
        }
        {
            typedef void (PCAGraphicsView::*_t)(double );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PCAGraphicsView::scaleChanged)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject PCAGraphicsView::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_PCAGraphicsView.data,
      qt_meta_data_PCAGraphicsView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *PCAGraphicsView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PCAGraphicsView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_PCAGraphicsView.stringdata0))
        return static_cast<void*>(const_cast< PCAGraphicsView*>(this));
    return QGraphicsView::qt_metacast(_clname);
}

int PCAGraphicsView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void PCAGraphicsView::nodeSelected(unsigned long long _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PCAGraphicsView::edgeSelected(unsigned long long _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PCAGraphicsView::scaleChanged(double _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
struct qt_meta_stringdata_TopicGraphicsView_t {
    QByteArrayData data[15];
    char stringdata0[205];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TopicGraphicsView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TopicGraphicsView_t qt_meta_stringdata_TopicGraphicsView = {
    {
QT_MOC_LITERAL(0, 0, 17), // "TopicGraphicsView"
QT_MOC_LITERAL(1, 18, 12), // "nodeSelected"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 5), // "_node"
QT_MOC_LITERAL(4, 38, 12), // "edgeSelected"
QT_MOC_LITERAL(5, 51, 5), // "_edge"
QT_MOC_LITERAL(6, 57, 12), // "scaleChanged"
QT_MOC_LITERAL(7, 70, 11), // "scale_value"
QT_MOC_LITERAL(8, 82, 18), // "scaleChangeHandler"
QT_MOC_LITERAL(9, 101, 11), // "applyLayout"
QT_MOC_LITERAL(10, 113, 12), // "_layout_name"
QT_MOC_LITERAL(11, 126, 16), // "_allow_free_drag"
QT_MOC_LITERAL(12, 143, 23), // "rubberBandChangeHandler"
QT_MOC_LITERAL(13, 167, 23), // "layoutTransitionHandler"
QT_MOC_LITERAL(14, 191, 13) // "highlightNode"

    },
    "TopicGraphicsView\0nodeSelected\0\0_node\0"
    "edgeSelected\0_edge\0scaleChanged\0"
    "scale_value\0scaleChangeHandler\0"
    "applyLayout\0_layout_name\0_allow_free_drag\0"
    "rubberBandChangeHandler\0layoutTransitionHandler\0"
    "highlightNode"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TopicGraphicsView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,
       6,    1,   60,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   63,    2, 0x0a /* Public */,
       9,    2,   66,    2, 0x0a /* Public */,
      12,    3,   71,    2, 0x0a /* Public */,
      13,    0,   78,    2, 0x0a /* Public */,
      14,    1,   79,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::ULongLong,    3,
    QMetaType::Void, QMetaType::ULongLong,    5,
    QMetaType::Void, QMetaType::Double,    7,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    7,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   10,   11,
    QMetaType::Void, QMetaType::QRect, QMetaType::QPointF, QMetaType::QPointF,    2,    2,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::ULongLong,    3,

       0        // eod
};

void TopicGraphicsView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TopicGraphicsView *_t = static_cast<TopicGraphicsView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->nodeSelected((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 1: _t->edgeSelected((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 2: _t->scaleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->scaleChangeHandler((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->applyLayout((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 5: _t->rubberBandChangeHandler((*reinterpret_cast< QRect(*)>(_a[1])),(*reinterpret_cast< QPointF(*)>(_a[2])),(*reinterpret_cast< QPointF(*)>(_a[3]))); break;
        case 6: _t->layoutTransitionHandler(); break;
        case 7: _t->highlightNode((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (TopicGraphicsView::*_t)(unsigned long long );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TopicGraphicsView::nodeSelected)) {
                *result = 0;
            }
        }
        {
            typedef void (TopicGraphicsView::*_t)(unsigned long long );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TopicGraphicsView::edgeSelected)) {
                *result = 1;
            }
        }
        {
            typedef void (TopicGraphicsView::*_t)(double );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TopicGraphicsView::scaleChanged)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject TopicGraphicsView::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_TopicGraphicsView.data,
      qt_meta_data_TopicGraphicsView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TopicGraphicsView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TopicGraphicsView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TopicGraphicsView.stringdata0))
        return static_cast<void*>(const_cast< TopicGraphicsView*>(this));
    return QGraphicsView::qt_metacast(_clname);
}

int TopicGraphicsView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void TopicGraphicsView::nodeSelected(unsigned long long _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TopicGraphicsView::edgeSelected(unsigned long long _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TopicGraphicsView::scaleChanged(double _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
