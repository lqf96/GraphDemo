/****************************************************************************
** Meta object code from reading C++ file 'gdpanel.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../gdpanel.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gdpanel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PCAPanel_t {
    QByteArrayData data[25];
    char stringdata0[315];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PCAPanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PCAPanel_t qt_meta_stringdata_PCAPanel = {
    {
QT_MOC_LITERAL(0, 0, 8), // "PCAPanel"
QT_MOC_LITERAL(1, 9, 12), // "scaleChanged"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 11), // "scale_value"
QT_MOC_LITERAL(4, 35, 13), // "layoutChanged"
QT_MOC_LITERAL(5, 49, 11), // "layout_name"
QT_MOC_LITERAL(6, 61, 15), // "allow_free_drag"
QT_MOC_LITERAL(7, 77, 13), // "highlightNode"
QT_MOC_LITERAL(8, 91, 5), // "_node"
QT_MOC_LITERAL(9, 97, 12), // "showNodeInfo"
QT_MOC_LITERAL(10, 110, 12), // "showEdgeInfo"
QT_MOC_LITERAL(11, 123, 5), // "_edge"
QT_MOC_LITERAL(12, 129, 9), // "goToNodeA"
QT_MOC_LITERAL(13, 139, 9), // "goToNodeB"
QT_MOC_LITERAL(14, 149, 18), // "scaleChangeHandler"
QT_MOC_LITERAL(15, 168, 24), // "sliderValueChangeWrapper"
QT_MOC_LITERAL(16, 193, 5), // "value"
QT_MOC_LITERAL(17, 199, 19), // "layoutChangeWrapper"
QT_MOC_LITERAL(18, 219, 10), // "switchMode"
QT_MOC_LITERAL(19, 230, 13), // "addSearchCond"
QT_MOC_LITERAL(20, 244, 15), // "clearSearchCond"
QT_MOC_LITERAL(21, 260, 11), // "searchNodes"
QT_MOC_LITERAL(22, 272, 20), // "showNodeInfoInSearch"
QT_MOC_LITERAL(23, 293, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(24, 310, 4) // "item"

    },
    "PCAPanel\0scaleChanged\0\0scale_value\0"
    "layoutChanged\0layout_name\0allow_free_drag\0"
    "highlightNode\0_node\0showNodeInfo\0"
    "showEdgeInfo\0_edge\0goToNodeA\0goToNodeB\0"
    "scaleChangeHandler\0sliderValueChangeWrapper\0"
    "value\0layoutChangeWrapper\0switchMode\0"
    "addSearchCond\0clearSearchCond\0searchNodes\0"
    "showNodeInfoInSearch\0QListWidgetItem*\0"
    "item"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PCAPanel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x06 /* Public */,
       4,    2,   92,    2, 0x06 /* Public */,
       7,    1,   97,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,  100,    2, 0x0a /* Public */,
      10,    1,  103,    2, 0x0a /* Public */,
      12,    0,  106,    2, 0x0a /* Public */,
      13,    0,  107,    2, 0x0a /* Public */,
      14,    1,  108,    2, 0x0a /* Public */,
      15,    1,  111,    2, 0x0a /* Public */,
      17,    1,  114,    2, 0x0a /* Public */,
      18,    0,  117,    2, 0x0a /* Public */,
      19,    0,  118,    2, 0x0a /* Public */,
      20,    0,  119,    2, 0x0a /* Public */,
      21,    0,  120,    2, 0x0a /* Public */,
      22,    1,  121,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,    5,    6,
    QMetaType::Void, QMetaType::ULongLong,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::ULongLong,    8,
    QMetaType::Void, QMetaType::ULongLong,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,   16,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 23,   24,

       0        // eod
};

void PCAPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PCAPanel *_t = static_cast<PCAPanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->scaleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->layoutChanged((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 2: _t->highlightNode((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 3: _t->showNodeInfo((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 4: _t->showEdgeInfo((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 5: _t->goToNodeA(); break;
        case 6: _t->goToNodeB(); break;
        case 7: _t->scaleChangeHandler((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->sliderValueChangeWrapper((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->layoutChangeWrapper((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->switchMode(); break;
        case 11: _t->addSearchCond(); break;
        case 12: _t->clearSearchCond(); break;
        case 13: _t->searchNodes(); break;
        case 14: _t->showNodeInfoInSearch((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PCAPanel::*_t)(double );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PCAPanel::scaleChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (PCAPanel::*_t)(QString , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PCAPanel::layoutChanged)) {
                *result = 1;
            }
        }
        {
            typedef void (PCAPanel::*_t)(unsigned long long );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PCAPanel::highlightNode)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject PCAPanel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_PCAPanel.data,
      qt_meta_data_PCAPanel,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *PCAPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PCAPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_PCAPanel.stringdata0))
        return static_cast<void*>(const_cast< PCAPanel*>(this));
    return QWidget::qt_metacast(_clname);
}

int PCAPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void PCAPanel::scaleChanged(double _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PCAPanel::layoutChanged(QString _t1, bool _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PCAPanel::highlightNode(unsigned long long _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
struct qt_meta_stringdata_TopicDocumentDialog_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TopicDocumentDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TopicDocumentDialog_t qt_meta_stringdata_TopicDocumentDialog = {
    {
QT_MOC_LITERAL(0, 0, 19) // "TopicDocumentDialog"

    },
    "TopicDocumentDialog"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TopicDocumentDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void TopicDocumentDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject TopicDocumentDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TopicDocumentDialog.data,
      qt_meta_data_TopicDocumentDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TopicDocumentDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TopicDocumentDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TopicDocumentDialog.stringdata0))
        return static_cast<void*>(const_cast< TopicDocumentDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int TopicDocumentDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_TopicPanel_t {
    QByteArrayData data[26];
    char stringdata0[330];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TopicPanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TopicPanel_t qt_meta_stringdata_TopicPanel = {
    {
QT_MOC_LITERAL(0, 0, 10), // "TopicPanel"
QT_MOC_LITERAL(1, 11, 12), // "scaleChanged"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 11), // "scale_value"
QT_MOC_LITERAL(4, 37, 13), // "layoutChanged"
QT_MOC_LITERAL(5, 51, 11), // "layout_name"
QT_MOC_LITERAL(6, 63, 15), // "allow_free_drag"
QT_MOC_LITERAL(7, 79, 13), // "highlightNode"
QT_MOC_LITERAL(8, 93, 5), // "_node"
QT_MOC_LITERAL(9, 99, 12), // "showNodeInfo"
QT_MOC_LITERAL(10, 112, 12), // "showEdgeInfo"
QT_MOC_LITERAL(11, 125, 5), // "_edge"
QT_MOC_LITERAL(12, 131, 12), // "showDocument"
QT_MOC_LITERAL(13, 144, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(14, 161, 4), // "item"
QT_MOC_LITERAL(15, 166, 9), // "goToNodeA"
QT_MOC_LITERAL(16, 176, 9), // "goToNodeB"
QT_MOC_LITERAL(17, 186, 18), // "scaleChangeHandler"
QT_MOC_LITERAL(18, 205, 24), // "sliderValueChangeWrapper"
QT_MOC_LITERAL(19, 230, 5), // "value"
QT_MOC_LITERAL(20, 236, 19), // "layoutChangeWrapper"
QT_MOC_LITERAL(21, 256, 10), // "switchMode"
QT_MOC_LITERAL(22, 267, 13), // "addSearchCond"
QT_MOC_LITERAL(23, 281, 15), // "clearSearchCond"
QT_MOC_LITERAL(24, 297, 11), // "searchNodes"
QT_MOC_LITERAL(25, 309, 20) // "showNodeInfoInSearch"

    },
    "TopicPanel\0scaleChanged\0\0scale_value\0"
    "layoutChanged\0layout_name\0allow_free_drag\0"
    "highlightNode\0_node\0showNodeInfo\0"
    "showEdgeInfo\0_edge\0showDocument\0"
    "QListWidgetItem*\0item\0goToNodeA\0"
    "goToNodeB\0scaleChangeHandler\0"
    "sliderValueChangeWrapper\0value\0"
    "layoutChangeWrapper\0switchMode\0"
    "addSearchCond\0clearSearchCond\0searchNodes\0"
    "showNodeInfoInSearch"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TopicPanel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x06 /* Public */,
       4,    2,   97,    2, 0x06 /* Public */,
       7,    1,  102,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,  105,    2, 0x0a /* Public */,
      10,    1,  108,    2, 0x0a /* Public */,
      12,    1,  111,    2, 0x0a /* Public */,
      15,    0,  114,    2, 0x0a /* Public */,
      16,    0,  115,    2, 0x0a /* Public */,
      17,    1,  116,    2, 0x0a /* Public */,
      18,    1,  119,    2, 0x0a /* Public */,
      20,    1,  122,    2, 0x0a /* Public */,
      21,    0,  125,    2, 0x0a /* Public */,
      22,    0,  126,    2, 0x0a /* Public */,
      23,    0,  127,    2, 0x0a /* Public */,
      24,    0,  128,    2, 0x0a /* Public */,
      25,    1,  129,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,    5,    6,
    QMetaType::Void, QMetaType::ULongLong,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::ULongLong,    8,
    QMetaType::Void, QMetaType::ULongLong,   11,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,

       0        // eod
};

void TopicPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TopicPanel *_t = static_cast<TopicPanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->scaleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->layoutChanged((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 2: _t->highlightNode((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 3: _t->showNodeInfo((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 4: _t->showEdgeInfo((*reinterpret_cast< unsigned long long(*)>(_a[1]))); break;
        case 5: _t->showDocument((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 6: _t->goToNodeA(); break;
        case 7: _t->goToNodeB(); break;
        case 8: _t->scaleChangeHandler((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->sliderValueChangeWrapper((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->layoutChangeWrapper((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->switchMode(); break;
        case 12: _t->addSearchCond(); break;
        case 13: _t->clearSearchCond(); break;
        case 14: _t->searchNodes(); break;
        case 15: _t->showNodeInfoInSearch((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (TopicPanel::*_t)(double );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TopicPanel::scaleChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (TopicPanel::*_t)(QString , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TopicPanel::layoutChanged)) {
                *result = 1;
            }
        }
        {
            typedef void (TopicPanel::*_t)(unsigned long long );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TopicPanel::highlightNode)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject TopicPanel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TopicPanel.data,
      qt_meta_data_TopicPanel,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TopicPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TopicPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TopicPanel.stringdata0))
        return static_cast<void*>(const_cast< TopicPanel*>(this));
    return QWidget::qt_metacast(_clname);
}

int TopicPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void TopicPanel::scaleChanged(double _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TopicPanel::layoutChanged(QString _t1, bool _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TopicPanel::highlightNode(unsigned long long _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
