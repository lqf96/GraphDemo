#include "gdlayout.h"

//* Constants
//Elastic force stable length
const double ELASTIC_STABLE_LENGTH = 40;
//Elastic force K value
const double ELASTIC_K = 2;
//Friction K value
const double FRICTION_K = 0.2;
//Energy threshold value
const double ENERGY_THRESHOLD = 10;
//Node mass
const double NODE_MASS = 50;

//Force directed layout strategy class
//Constructor
ForceDirectedLayoutStrategy::ForceDirectedLayoutStrategy(QSharedPointer<Graph> graph, unsigned int _fps)
	: GraphLayoutStrategy(graph)
{
	using namespace std::placeholders;

	this->fps = _fps;

	//Create a new layout animation state
	this->newState(std::bind(ElasticForce, _1, _2, _3, _4, ELASTIC_STABLE_LENGTH, ELASTIC_K));
}

//Graph is stable or not
bool ForceDirectedLayoutStrategy::isStable()
{
	return this->stable;
}

//Reset and establish layout animation state
void ForceDirectedLayoutStrategy::newState(std::function<bool(Edge*, double*, double*, double*)> _edge_force_func,
	std::function<bool(Node*, Node*, double*, double*, double*)> _node_force_func)
{
	if ((_edge_force_func == nullptr) && (_node_force_func == nullptr))
		throw QString("ArgumentsNotEnough");

	this->edge_force_func = _edge_force_func;
	this->node_force_func = _node_force_func;

	QList<Node*> nodes = this->getGraph()->getNodes();
	for (auto node : nodes)
	{
		TransitionInfo node_state;

		node_state.ax = 0;
		node_state.ay = 0;
		node_state.az = 0;
		node_state.vx = 0;
		node_state.vy = 0;
		node_state.vz = 0;

		this->layout_info[node] = node_state;
	}

	this->stable = false;
	this->total_energy = INT_MAX;
}

//Run layout strategy
void ForceDirectedLayoutStrategy::run()
{
	if (stable)
		return;

	QList<Node*> nodes = this->getGraph()->getNodes();
	QList<Edge*> edges = this->getGraph()->getEdges();
	double dt = 1.0 / this->fps;
	unsigned int i, j;
	double _total_energy = 0;

	if (this->node_force_func != nullptr)
		for (i = 0; i < nodes.size(); i++)
			for (j = i + 1; j < nodes.size(); j++)
			{
				double fx, fy, fz;
				auto node_a = nodes[i], node_b = nodes[j];
				TransitionInfo new_state_a, new_state_b;

				bool effective = this->node_force_func(node_a, node_b, &fx, &fy, &fz);
				if (!effective)
					continue;

				new_state_a = this->layout_info[node_a];
				new_state_b = this->layout_info[node_b];
				new_state_a.ax += fx / NODE_MASS;
				new_state_a.ay += fy / NODE_MASS;
				new_state_a.az += fz / NODE_MASS;
				new_state_b.ax -= fx / NODE_MASS;
				new_state_b.ay -= fy / NODE_MASS;
				new_state_b.az -= fz / NODE_MASS;
				this->layout_info[node_a] = new_state_a;
				this->layout_info[node_b] = new_state_b;
			}

	if (this->edge_force_func != nullptr)
		for (auto edge : edges)
		{
			double fx, fy, fz;
			auto node_a = edge->getNodeA(), node_b = edge->getNodeB();
			TransitionInfo new_state_a, new_state_b;

			bool effective = this->edge_force_func(edge, &fx, &fy, &fz);
			if (!effective)
				continue;

			new_state_a = this->layout_info[node_a];
			new_state_b = this->layout_info[node_b];
			new_state_a.ax += fx / NODE_MASS;
			new_state_a.ay += fy / NODE_MASS;
			new_state_a.az += fz / NODE_MASS;
			new_state_b.ax -= fx / NODE_MASS;
			new_state_b.ay -= fy / NODE_MASS;
			new_state_b.az -= fz / NODE_MASS;
			this->layout_info[node_a] = new_state_a;
			this->layout_info[node_b] = new_state_b;
		}

	for (auto node : nodes)
	{
		//Get node state info
		TransitionInfo node_info = this->layout_info[node];

		//Friction included
		node_info.vx += (node_info.ax - node_info.vx * FRICTION_K) * dt;
		node_info.vy += (node_info.ay - node_info.vy * FRICTION_K) * dt;
		node_info.vz += (node_info.az - node_info.vz * FRICTION_K) * dt;
		//Position
		node->prop("x", node->prop<double>("x") + node_info.vx * dt);
		node->prop("y", node->prop<double>("y") + node_info.vy * dt);
		node->prop("z", node->prop<double>("z") + node_info.vz * dt);

		//Total kintic energy
		_total_energy += 0.5 * (node_info.vx * node_info.vx + node_info.vy * node_info.vy + node_info.vz * node_info.vz) * NODE_MASS;

		//Clear force and set node state info
		node_info.ax = 0;
		node_info.ay = 0;
		node_info.az = 0;
		this->layout_info[node] = node_info;
	}

	if (std::abs(_total_energy - this->total_energy) < ENERGY_THRESHOLD)
		this->stable = true;
	else
		this->total_energy = _total_energy;
}

//Get force function
std::function<bool(Edge*, double*, double*, double*)> ForceDirectedLayoutStrategy::getEdgeForceFunc()
{
	return this->edge_force_func;
}

std::function<bool(Node*, Node*, double*, double*, double*)> ForceDirectedLayoutStrategy::getNodeForceFunc()
{
	return this->node_force_func;
}

//Force directed layout force functions
//Elastic force
bool ElasticForce(Edge* edge, double* fx, double* fy, double* fz, double stable_length, double k)
{
	Node* node1 = edge->getNodeA();
	Node* node2 = edge->getNodeB();

	double dx = node2->prop<double>("x") - node1->prop<double>("x"),
		dy = node2->prop<double>("y") - node1->prop<double>("y"),
		dz = node2->prop<double>("z") - node1->prop<double>("z");
	double l = std::sqrt(dx * dx + dy * dy + dz * dz);

	*fx = (l - stable_length) * (dx / l) * k;
	*fy = (l - stable_length) * (dy / l) * k;
	*fz = (l - stable_length) * (dz / l) * k;

	return true;
}