#pragma once

#include <cmath>
#include <functional>
#include <qgraphicsview.h>
#include <qgraphicsscene.h>
#include <qgraphicsitem.h>
#include <qlist.h>
#include <qobject.h>
#include <qcolor.h>
#include <qrect.h>
#include <qvariant.h>
#include <qbrush.h>
#include <qsharedpointer.h>
#include <qwidget.h>
#include <qevent.h>
#include <qgraphicssceneevent.h>
#include <qpoint.h>
#include <qtimer.h>
#include "graph.h"
#include "pcagraph.h"
#include "topicgraph.h"
#include "gdlayout.h"

//* Constants
//Node graphics item radius
const double NODE_ITEM_RADIUS = 4;
//Edge width
const double EDGE_WIDTH = 1;
//Wheel event scale factor
const double SCALE_FACTOR = 500;
//Scale range
const double UPPER_SCALE_RANGE = 4;
const double LOWER_SCALE_RANGE = -1;
//Frame per second (Layout transition animation)
const unsigned int FPS = 60;
//Custom property keys
const int HIGHLIGHT_HANDLER = 0;
const int UNSET_HIGHLIGHT_HANDLER = 1;
const int __MULTI_SELECTED = 2;

//* Shared constants
//Random range
extern const unsigned int RND_RANGE;
//Selected range border width
extern const double SELECTED_RANGE_WIDTH;
//Free drag factor
extern const unsigned int FREE_DRAG_FACTOR;

//* Helper functions
//Min / Max function
template <typename T>
T min(T a, T b)
{
	return (a < b) ? a : b;
}

template <typename T>
T max(T a, T b)
{
	return (a > b) ? a : b;
}

//Class declarations
class PCAGraphicsView;
class PCAGraphicsNodeItem;
class PCAGraphicsEdgeItem;
class TopicGraphicsView;
class TopicGraphicsNodeItem;
class TopicGraphicsEdgeItem;

//PCA graphics node item
class PCAGraphicsNodeItem : public QGraphicsEllipseItem
{private:
	//PCA graphics view reference
	PCAGraphicsView* self;
	//Node reference
	Node* node;

	//Mouse status
	enum MouseState
	{
		NotPressed = 0,
		Pressed,
		Moved
	} mouse_status;
protected:
	//Hover enter and leave event
	virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* e);
	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* e);
	//Mouse pressed, move and release event
	virtual void mousePressEvent(QGraphicsSceneMouseEvent* e);
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* e);
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* e);
	//Graphics item change
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);
public:
	//Constructor
	PCAGraphicsNodeItem(PCAGraphicsView* _self, Node* _node);

	//Move node
	void moveNode(double dx, double dy);
	void moveNode();
};

//PCA graphics edge item
class PCAGraphicsEdgeItem : public QGraphicsLineItem
{private:
	//PCA graphics view reference
	PCAGraphicsView* self;
	//Edge reference
	Edge* edge;

	//QPen object
	QPen _pen;

	//Single selection flag
	bool single_flag;
protected:
	//Hover enter and leave event
	virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* e);
	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* e);
	//Mouse pressed event
	virtual void mousePressEvent(QGraphicsSceneMouseEvent* e);
	//Graphics item change
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);
public:
	//Constructor
	PCAGraphicsEdgeItem(PCAGraphicsView* _self, Edge* _edge);

	//Move edge
	void moveEdge();
};

//PCA graphics view
class PCAGraphicsView : public QGraphicsView
{
	Q_OBJECT
private:
	//Graph reference
	QSharedPointer<PCAGraph> graph;

	//Scale status
	double scale_status;
	//Scale at view side or panel side
	bool scale_at_view_side;

	//Layout transition timer
	QTimer* layout_timer;
protected:
	//Mouse wheel event
	void wheelEvent(QWheelEvent* e);
	//Key press and release event
	void keyPressEvent(QKeyEvent* e);
	void keyReleaseEvent(QKeyEvent* e);
public:
	//Graphics scene
	QGraphicsScene* pca_scene;

	//Layout strategy
	GraphLayoutStrategy* layout;

	//Allow free drag
	bool allow_free_drag;
	//Free drag state reset handler
	std::function<void(GraphLayoutStrategy*)> state_reset_handler;

	//Constructor
	PCAGraphicsView(QSharedPointer<PCAGraph> _graph, QWidget* parent = 0);

	//2D environment initialization
	void init2D();
	//3D environment initialization
	//void init3D();

	//Qt signal wrapper
	void nodeSelectedWrapper(unsigned long long _node);
	void edgeSelectedWrapper(unsigned long long _edge);
public slots:
	//Scale change handler
	void scaleChangeHandler(double scale_value);
	//Apply layout
	void applyLayout(QString _layout_name, bool _allow_free_drag);
	//Layout transition handler
	void layoutTransitionHandler();
	//Rubber band change handler
	void rubberBandChangeHandler(QRect, QPointF, QPointF);
	//Highlight node
	void highlightNode(unsigned long long _node);
signals:
	//Node selected
	void nodeSelected(unsigned long long _node);
	//Edge selected
	void edgeSelected(unsigned long long _edge);
	//Scale changed
	void scaleChanged(double scale_value);
};

//Topic graphics node item
class TopicGraphicsNodeItem : public QGraphicsEllipseItem
{private:
	//PCA graphics view reference
	TopicGraphicsView* self;
	//Node reference
	Node* node;

	//Mouse status
	enum MouseState
	{
		NotPressed = 0,
		Pressed,
		Moved
	} mouse_status;
protected:
	//Hover enter and leave event
	virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* e);
	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* e);
	//Mouse pressed, move and release event
	virtual void mousePressEvent(QGraphicsSceneMouseEvent* e);
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* e);
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* e);
	//Graphics item change
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);
public:
	//Constructor
	TopicGraphicsNodeItem(TopicGraphicsView* _self, Node* _node, unsigned int max_node_conn);

	//Move node
	void moveNode(double dx, double dy);
	void moveNode();
};

//Topic graphics edge item
class TopicGraphicsEdgeItem : public QGraphicsLineItem
{private:
	//PCA graphics view reference
	TopicGraphicsView* self;
	//Edge reference
	Edge* edge;

	//QPen object
	QPen _pen;

	//Single selection flag
	bool single_flag;
protected:
	//Hover enter and leave event
	virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* e);
	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* e);
	//Mouse pressed event
	virtual void mousePressEvent(QGraphicsSceneMouseEvent* e);
	//Graphics item change
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);
public:
	//Constructor
	TopicGraphicsEdgeItem(TopicGraphicsView* _self, Edge* _edge);

	//Move edge
	void moveEdge();
};

//Topic graphics view
class TopicGraphicsView : public QGraphicsView
{
	Q_OBJECT
private:
	//Graph reference
	QSharedPointer<TopicGraph> graph;

	//Scale at view side or panel side
	bool scale_at_view_side;
	//Scale status
	double scale_status;

	//Layout transition timer
	QTimer* layout_timer;
protected:
	//Mouse wheel event
	void wheelEvent(QWheelEvent* e);
	//Key press and release event
	void keyPressEvent(QKeyEvent* e);
	void keyReleaseEvent(QKeyEvent* e);
public:
	//Graphics scene
	QGraphicsScene* topic_scene;

	//Layout strategy
	GraphLayoutStrategy* layout;

	//Allow free drag
	bool allow_free_drag;
	//Free drag state reset handler
	std::function<void(GraphLayoutStrategy*)> state_reset_handler;

	//Constructor
	TopicGraphicsView(QSharedPointer<TopicGraph> _graph, QWidget* parent = 0);

	//2D environment initialization
	void init2D();
	//3D environment initialization
	//void init3D();

	//Qt signal wrapper
	void nodeSelectedWrapper(unsigned long long _node);
	void edgeSelectedWrapper(unsigned long long _edge);
public slots:
	//Scale change handler
	void scaleChangeHandler(double scale_value);
	//Apply layout
	void applyLayout(QString _layout_name, bool _allow_free_drag);
	//Rubber band change handler
	void rubberBandChangeHandler(QRect, QPointF, QPointF);
	//Layout transition handler
	void layoutTransitionHandler();
	//Highlight node
	void highlightNode(unsigned long long _node);
signals:
	//Node selected
	void nodeSelected(unsigned long long _node);
	//Edge selected
	void edgeSelected(unsigned long long _edge);
	//Scale changed
	void scaleChanged(double scale_value);
};