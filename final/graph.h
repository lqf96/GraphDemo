#pragma once

#include <functional>
#include <qstring.h>
#include <qlist.h>
#include <qvariant.h>
#include <qhash.h>
#include <qsharedpointer.h>

//Dynamic type helper
//Variable table
#define DYN_PROP_TABLE			QVariantHash __prop__; \
								template <typename T> \
								T prop(QString name) \
								{ \
									return this->__prop__[name].value<T>(); \
								} \
								template <typename T> \
								void prop(QString name, T value) \
								{ \
									this->__prop__[name].setValue<T>(value); \
								}

//Class declarations
class Node;
class Edge;
class Graph;

//Abstract node class
class Node
{private:
	//Friend classes
	friend class Graph;

	//Graph reference
	Graph* graph;
public:
	DYN_PROP_TABLE

	//Constructor
	Node();

	//Get graph reference
	Graph* getGraph();

	//Connect to another node
	Edge* connect(Node* node, Edge** p_edge2 = 0);
	//Get connected edges and nodes
	QList<Edge*> getConnectedEdges(bool source = true, std::function<bool(Edge*)> func = [](Edge*){	return true;});
	QList<Node*> getConnectedNodes(bool source = true, std::function<bool(Node*)> func = [](Node*){	return true;});
};

//Abstract edge class
class Edge
{private:
	//Friend classes
	friend class Graph;

	//Graph reference
	Graph* graph;
	//Connected nodes
	Node* node_a;
	Node* node_b;
public:
	DYN_PROP_TABLE

	//Constructor
	Edge();

	//Get effective edge (For non-directed graph)
	Edge* getEffectiveEdge();

	//Get graph reference
	Graph* getGraph();
	//Connected nodes
	Node* getNodeA();
	Node* getNodeB();
};

//Abstract graph class
class Graph
{
	//Friend classes
	friend class Node;
private:
	//Nodes
	QList<Node*> nodes;
	//Adjacency matrix
	QList<QList<Edge*>> adj_matrix;

	//Directed or non-directed edge
	bool directed;
public:
	//Constructor
	Graph(bool _directed = true);
	//Destructor
	~Graph();

	//Directed or non-directed edge
	bool isDirected();

	//Add node
	void addNode(Node* node);
	//Remove node
	void removeNode(Node* node, bool release = true);
	//Remove edge
	void removeEdge(Edge* edge);
	//Connect two nodes
	Edge* connect(Node* node1, Node* node2, Edge** p_edge2 = 0);

	//Filter and get node collection (All nodes by default)
	QList<Node*> getNodes(std::function<bool(Node*)> func = [](Node*){	return true;});
	//Filter and get edge collection (All edges by default for directed graph, non-duplicated result for non-directed graph)
	QList<Edge*> getEdges(std::function<bool(Edge*)> func = [](Edge*){	return true;}, bool no_duplicated_edge = true);
};

//Abstract graph layout strategy class
class GraphLayoutStrategy
{private:
	//Graph reference
	QSharedPointer<Graph> graph;
public:
	//Constructor
	GraphLayoutStrategy(QSharedPointer<Graph> _graph);

	//Get and set graph
	void setGraph(QSharedPointer<Graph> _graph);
	QSharedPointer<Graph> getGraph();

	//Run layout strategy
	virtual void run() = 0;
	//Graph is stable or not
	virtual bool isStable() = 0;
};