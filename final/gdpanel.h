#pragma once

#include <functional>
#include <qwidget.h>
#include <qlayout.h>
#include <qfont.h>
#include <qpalette.h>
#include <qpushbutton.h>
#include <qslider.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qtablewidget.h>
#include <qlistwidget.h>
#include <qsplitter.h>
#include <qgridlayout.h>
#include <qheaderview.h>
#include <qregexp.h>
#include <qdialog.h>
#include <qtextedit.h>
#include <qsharedpointer.h>
#include <qlist.h>
#include <qvariant.h>
#include <qhash.h>
#include <qlineedit.h>
#include <qpair.h>
#include <qmessagebox.h>
#include "graph.h"
#include "pcagraph.h"
#include "topicgraph.h"
#include "bsui.h"

//* Constants
//Slider value factor
const int SLIDER_FACTOR = 100;

//* Shared constants
//Welcome message
extern const QString WELCOME_MSG;
//Node info panel title
extern const QString NODE_TITLE;
//Edge panel go to node message
extern const QString GO_TO_NODE;
//Scale range
const double _UPPER_SCALE_RANGE = 4;
const double _LOWER_SCALE_RANGE = -1;
//Panel mode string
const QString TO_SEARCH_MODE = "> Search";
const QString TO_NORMAL_MODE = "> Normal";
//Search result node reference
const int SEARCH_RESULT_NODE_REF = INT_MAX;

//* Helper functions & definitions
//Qt tuple
typedef QList<QVariant> QTuple;

//Make Qt tuple
template <typename T>
QTuple makeQtTuple(T arg)
{
	QList<QVariant> result;
	QVariant _arg;

	_arg.setValue<T>(arg);
	result.push_front(_arg);
	return result;
}

template <typename T, typename... ArgType>
QTuple makeQtTuple(T arg, ArgType... args)
{
	QList<QVariant> result = makeQtTuple(args...);
	QVariant _arg;

	_arg.setValue<T>(arg);
	result.push_front(_arg);
	return result;
}

//PCA graph panel
class PCAPanel : public QWidget
{
	Q_OBJECT
private:
	//Global components
	QPushButton* panel_mode;
	QSlider* view_slider;
	QComboBox* display_mode;
	QComboBox* layout_strategy_combo;
	QVBoxLayout* global_layout;

	//Welcome panel
	QWidget* welcome_panel;
	QLabel* welcome_title;
	QLabel* welcome_msg;

	//Node info panel
	QWidget* node_info_panel;
	QLabel* node_title;
	QLabel* node_type;
	QTableWidget* prop_table;

	//Edge info panel
	QWidget* edge_info_panel;
	QLabel* edge_title;
	QLabel* edge_msg;
	QPushButton* go_to_node1;
	QPushButton* go_to_node2;

	//Search / Filter panel
	QWidget* search_panel;
	QLabel* search_title;
	QTableWidget* search_cond_table;
	QPushButton* search_btn;
	QPushButton* new_cond_btn;
	QPushButton* clear_cond_btn;
	QListWidget* search_result;

	//Multiple node / edge panel
	QWidget* multiple_node_panel;
	QLabel* multiple_node_info;
	QListView* multiple_node_list;
	QTableWidget* multi_node_prop_table;

	//Current panel
	QWidget* current_panel;

	//[Edge info panel] Connected nodes
	Node* node_a;
	Node* node_b;

	//[Global components] Search mode or normal node
	bool search_mode;
	//[Global components] Panel reference for the previous panel
	QWidget* previous_panel;

	//[Search panel] Condition parsers
	static QList<std::function<QVariant(Node*, QString, QString)>> cond_parsers;

	//[Search panel] PCA graph reference
	QSharedPointer<PCAGraph> graph;
public:
	//Constructor
	PCAPanel(QSharedPointer<PCAGraph> _graph);
	//Destructor
	~PCAPanel();

	//[Search panel] Initialize Condition parsers
	static void initCondParsers();
public slots:
	//Show node information
	void showNodeInfo(unsigned long long _node);
	//Show edge information
	void showEdgeInfo(unsigned long long _edge);
	//[Edge info panel] Go to node A or B
	void goToNodeA();
	void goToNodeB();
	//[Global components] Scale change handler
	void scaleChangeHandler(double scale_value);
	//[Global components] Slider value change wrapper
	void sliderValueChangeWrapper(int value);
	//[Global components] Layout change wrapper
	void layoutChangeWrapper(int);
	//[Global components] Switch between normal mode and search mode
	void switchMode();
	//[Search panel] Add or clear search conditions
	void addSearchCond();
	void clearSearchCond();
	//[Search panel] Search for nodes
	void searchNodes();
	//[Search panel] Show node information
	void showNodeInfoInSearch(QListWidgetItem* item);
signals:
	//Scale changed
	void scaleChanged(double scale_value);
	//Layout changed
	void layoutChanged(QString layout_name, bool allow_free_drag);
	//Highlight node
	void highlightNode(unsigned long long _node);
};

//Topic document dialog
class TopicDocumentDialog : public QDialog
{
	Q_OBJECT
private:
	//Dialog components
	QLabel* title_label;
	QTextEdit* content_zone;
public:
	//Constructor
	TopicDocumentDialog();
	
	//Show document
	void showDocument(unsigned int doc_id, QString title, QString& content);
};

//Topic graph panel
class TopicPanel : public QWidget
{
	Q_OBJECT
private:
	//Global components
	QPushButton* panel_mode;
	QSlider* view_slider;
	QComboBox* display_mode;
	QComboBox* layout_strategy_combo;
	QVBoxLayout* global_layout;

	//Welcome panel
	QWidget* welcome_panel;
	QLabel* welcome_title;
	QLabel* welcome_msg;

	//Node info panel
	QWidget* node_info_panel;
	QLabel* node_info_title;
	QLabel* node_label_1;
	QLabel* node_label_2;
	QListWidget* topic_word_list;
	QListWidget* topic_document_list;
	//(Topic document dialog)
	TopicDocumentDialog* doc_dialog;

	//Edge info panel
	QWidget* edge_info_panel;
	QLabel* edge_title;
	QLabel* edge_msg;
	QPushButton* go_to_node1;
	QPushButton* go_to_node2;

	//Search / Filter panel
	QWidget* search_panel;
	QLabel* search_title;
	QTableWidget* search_cond_table;
	QPushButton* search_btn;
	QPushButton* new_cond_btn;
	QPushButton* clear_cond_btn;
	QListWidget* search_result;

	//Multiple node / edge panel
	QWidget* multiple_node_panel;
	QLabel* multiple_node_info;
	QListView* multiple_node_list;
	QTableWidget* multi_node_prop_table;

	//Current panel
	QWidget* current_panel;

	//[Node info panel] Topic graph reference
	QSharedPointer<TopicGraph> graph;

	//[Edge info panel] Connected nodes
	Node* node_a;
	Node* node_b;

	//[Global components] Search mode or normal node
	bool search_mode;
	//[Global components] Panel reference for the previous panel
	QWidget* previous_panel;

	//[Search panel] Condition parsers
	static QHash<QString, std::function<QVariant(Node*, QString)>> cond_parsers;
public:
	//Constructor
	TopicPanel(QSharedPointer<TopicGraph> _graph);
	//Destructor
	~TopicPanel();

	//[Search panel] Initialize Condition parsers
	static void initCondParsers();
public slots:
	//Show node information
	void showNodeInfo(unsigned long long _node);
	//Show edge information
	void showEdgeInfo(unsigned long long _edge);
	//[Node info panel] Show document
	void showDocument(QListWidgetItem* item);
	//[Edge info panel] Go to node A or B
	void goToNodeA();
	void goToNodeB();
	//[Global components] Scale change handler
	void scaleChangeHandler(double scale_value);
	//[Global components] Slider value change wrapper
	void sliderValueChangeWrapper(int value);
	//[Global components] Layout change wrapper
	void layoutChangeWrapper(int);
	//[Global components] Switch between normal mode and search mode
	void switchMode();
	//[Search panel] Add or clear search conditions
	void addSearchCond();
	void clearSearchCond();
	//[Search panel] Search for nodes
	void searchNodes();
	//[Search panel] Show node information
	void showNodeInfoInSearch(QListWidgetItem* item);
signals:
	//Scale changed
	void scaleChanged(double scale_value);
	//Layout changed
	void layoutChanged(QString layout_name, bool allow_free_drag);
	//Highlight node
	void highlightNode(unsigned long long _node);
};