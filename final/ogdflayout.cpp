#include "ogdflayout.h"

//OGDF layout final state class
//Constructor
OGDFLayoutFinalState::OGDFLayoutFinalState(QSharedPointer<Graph> _graph, OGDFLayoutFinalState::OGDFLayout ogdf_layout, double _scale_factor)
{
	//Create OGDF nodes
	for (auto node : _graph->getNodes())
		this->node_table[node] = this->ogdf_graph.newNode();
	//Connect nodes
	for (auto edge : _graph->getEdges())
		this->ogdf_graph.newEdge(this->node_table[edge->getNodeA()], this->node_table[edge->getNodeB()]);

	//Initialize OGDF graph attribute
	this->ogdf_ga = ogdf::GraphAttributes(this->ogdf_graph);
	
	//Calculate layout
	//FMMM layout
	if (ogdf_layout == OGDFLayout::FMMMLayout)
	{
		ogdf::FMMMLayout fmmm_layout;
		fmmm_layout.call(this->ogdf_ga);
	}
	//Planarization layout
	else if (ogdf_layout == OGDFLayout::TutteLayout)
	{
		ogdf::TutteLayout tutte_layout;
		tutte_layout.call(this->ogdf_ga);
	}

	//Set scale factor
	this->scale_factor = _scale_factor;
}

//Function call entry
void OGDFLayoutFinalState::operator ()(Node* node, double* dx, double* dy, double* dz)
{
	ogdf::node ogdf_node = this->node_table[node];

	*dx = this->ogdf_ga.x(ogdf_node) * this->scale_factor;
	*dy = this->ogdf_ga.y(ogdf_node) * this->scale_factor;
	*dz = 0;

	//Uncomment this only when 3D support is enabled for OGDF!
	//*dz = this->ogdf_ga.z(ogdf_node);
}