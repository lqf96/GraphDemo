#include "graph.h"

//Abstract graph layout strategy class
//Constructor
GraphLayoutStrategy::GraphLayoutStrategy(QSharedPointer<Graph> _graph)
{
	this->graph = _graph;
}

//Set graph
void GraphLayoutStrategy::setGraph(QSharedPointer<Graph> _graph)
{
	this->graph = _graph;
}

//Get graph
QSharedPointer<Graph> GraphLayoutStrategy::getGraph()
{
	return this->graph;
}