#include "gdpanel.h"

//* Constants
//Edge info message (non-directed)
const QString EDGE_MSG_NON_DIRECTED = "<p style='line-height: 20px; padding: 0px;'>"
	"This edge connects node #%1 and node #%2.<br />"
	"The weight of the edge is %3.</p>";
//Document ID (Custom data key)
const int DOC_ID = INT_MAX;

//* Topic document dialog class
//Constructor
TopicDocumentDialog::TopicDocumentDialog()
{
	//Components and layout
	this->title_label = new QLabel();
	this->content_zone = new QTextEdit();
	QVBoxLayout* layout = new QVBoxLayout();

	//Set window
	this->setFixedWidth(400);
	//Set components
	this->title_label->setFont(BSUI::BSUIFont(24));
	this->title_label->setPalette(BSUI::BSUIFontColor());
	this->title_label->setWordWrap(true);
	this->content_zone->setFont(BSUI::BSUIFont(14));
	this->content_zone->setReadOnly(true);
	//Configure layout and add components
	layout->setMargin(10);
	layout->addWidget(title_label);
	layout->addWidget(content_zone);
	//Set layout
	this->setLayout(layout);
}

//Show document
void TopicDocumentDialog::showDocument(unsigned int doc_id, QString title, QString& content)
{
	//Set window title
	this->setWindowTitle("Document #" + QString::number(doc_id));
	//Set title and content
	this->title_label->setText(title);
	this->content_zone->setHtml("<p style='line-height: 20px; color: #333'>" + content + "</p>");
	this->content_zone->adjustSize();

	//Show window
	this->show();
}

//Topic graph panel class
//Static variables
//[Search panel] Condition parsers
QHash<QString, std::function<QVariant(Node*, QString)>> TopicPanel::cond_parsers;

//Constructor
TopicPanel::TopicPanel(QSharedPointer<TopicGraph> _graph)
{
	//* Initialize components
	//Global components
	this->panel_mode = new QPushButton(TO_SEARCH_MODE);
	this->view_slider = new QSlider(Qt::Horizontal);
	this->display_mode = new QComboBox();
	this->layout_strategy_combo = new QComboBox();
	//Welcome panel
	this->welcome_panel = new QWidget();
	this->welcome_title = new QLabel("Welcome");
	this->welcome_msg = new QLabel(WELCOME_MSG);
	//Node info panel
	this->node_info_panel = new QWidget();
	this->node_info_title = new QLabel(NODE_TITLE);
	this->node_label_1 = new QLabel("Topic words:");
	this->node_label_2 = new QLabel("Topic documents:");
	this->topic_word_list = new QListWidget();
	this->topic_document_list = new QListWidget();
	this->doc_dialog = new TopicDocumentDialog();
	//Edge info panel
	this->edge_info_panel = new QWidget();
	this->edge_title = new QLabel("Edge");
	this->edge_msg = new QLabel(EDGE_MSG_NON_DIRECTED);
	this->go_to_node1 = new QPushButton(GO_TO_NODE);
	this->go_to_node2 = new QPushButton(GO_TO_NODE);
	//Search panel
	this->search_panel = new QWidget();
	this->search_title = new QLabel("Search");
	this->search_cond_table = new QTableWidget(0, 2);
	this->new_cond_btn = new QPushButton("Add");
	this->clear_cond_btn = new QPushButton("Clear");
	this->search_btn = new QPushButton("Search");
	this->search_result = new QListWidget();

	//* Component configuration
	//Global components
	this->panel_mode->adjustSize();
	this->view_slider->setRange((int)(_LOWER_SCALE_RANGE * SLIDER_FACTOR), (int)(_UPPER_SCALE_RANGE * SLIDER_FACTOR));
	this->display_mode->setFixedWidth(50);
	this->display_mode->addItem("2D");
	this->display_mode->addItem("3D");
	this->layout_strategy_combo->addItem("Random", makeQtTuple(QString("Random"), false));
	this->layout_strategy_combo->addItem("Circular", makeQtTuple(QString("Circular"), false));
	this->layout_strategy_combo->addItem("(OGDF) FMMM", makeQtTuple(QString("OGDFFMMM"), false));
	this->layout_strategy_combo->addItem("(OGDF) Tutte", makeQtTuple(QString("OGDFTutte"), false));
	this->layout_strategy_combo->addItem("Force (Elastic)", makeQtTuple(QString("ElasticForce"), true));
	//Welcome panel components
	this->welcome_title->setFont(BSUI::BSUIFont(24));
	this->welcome_title->setPalette(BSUI::BSUIFontColor());
	this->welcome_msg->setFont(BSUI::BSUIFont(14));
	this->welcome_msg->setPalette(BSUI::BSUIFontColor());
	this->welcome_msg->setWordWrap(true);
	//Node info panel components
	this->node_info_title->setFont(BSUI::BSUIFont(24));
	this->node_info_title->setPalette(BSUI::BSUIFontColor());
	this->node_label_1->setFont(BSUI::BSUIFont(14));
	this->node_label_1->setPalette(BSUI::BSUIFontColor());
	this->node_label_2->setFont(BSUI::BSUIFont(14));
	this->node_label_2->setPalette(BSUI::BSUIFontColor());
	this->go_to_node1->setMaximumWidth(150);
	this->go_to_node2->setMaximumWidth(150);
	//Edge info panel
	this->edge_title->setFont(BSUI::BSUIFont(24));
	this->edge_title->setPalette(BSUI::BSUIFontColor());
	this->edge_msg->setFont(BSUI::BSUIFont(14));
	this->edge_msg->setPalette(BSUI::BSUIFontColor());
	this->edge_msg->setWordWrap(true);
	//Search panel
	this->search_title->setFont(BSUI::BSUIFont(24));
	this->search_title->setPalette(BSUI::BSUIFontColor());
	QList<QString> cond_table_headers;
	cond_table_headers.append("Property");
	cond_table_headers.append("Value");
	this->search_cond_table->setHorizontalHeaderLabels(cond_table_headers);
	this->search_cond_table->verticalHeader()->setVisible(false);

	//* Create layout and assemble components
	//Global layout
	global_layout = new QVBoxLayout();
	QHBoxLayout* global_line1 = new QHBoxLayout();
	QHBoxLayout* global_line2 = new QHBoxLayout();
	//Welcome panel layout
	QVBoxLayout* welcome_layout = new QVBoxLayout();
	//Assemble welcome panel
	welcome_layout->setMargin(0);
	welcome_layout->addSpacing(10);
	welcome_layout->addWidget(this->welcome_title);
	welcome_layout->addWidget(this->welcome_msg);
	welcome_layout->addStretch(1);
	//Node info panel layout
	QVBoxLayout* node_info_layout = new QVBoxLayout();
	//Assemble node info panel
	node_info_layout->setMargin(0);
	node_info_layout->addSpacing(10);
	node_info_layout->addWidget(this->node_info_title);
	node_info_layout->addWidget(this->node_label_1);
	node_info_layout->addWidget(this->topic_word_list, 1);
	node_info_layout->addWidget(this->node_label_2);
	node_info_layout->addWidget(this->topic_document_list, 1);
	//Edge info panel layout
	QVBoxLayout* edge_layout = new QVBoxLayout();
	//Assemble edge info panel
	edge_layout->setMargin(0);
	edge_layout->addSpacing(10);
	edge_layout->addWidget(this->edge_title);
	edge_layout->addWidget(this->edge_msg);
	edge_layout->addWidget(this->go_to_node1);
	edge_layout->addWidget(this->go_to_node2);
	edge_layout->addStretch(1);
	//Search panel layout
	QGridLayout* search_layout = new QGridLayout();
	//Assemble search panel
	search_layout->setMargin(0);
	search_layout->addItem(new QSpacerItem(0, 10), 0, 0, 1, 2);
	search_layout->addWidget(this->search_title, 1, 0, 1, 2);
	search_layout->addWidget(this->search_cond_table, 2, 0, 1, 2);
	search_layout->addWidget(this->new_cond_btn, 3, 0);
	search_layout->addWidget(this->clear_cond_btn, 3, 1);
	search_layout->addWidget(this->search_btn, 4, 0, 1, 2);
	search_layout->addWidget(this->search_result, 5, 0, 1, 2);
	search_layout->setRowStretch(2, 1);
	search_layout->setRowStretch(5, 1);
	//Apply panel layout
	this->welcome_panel->setLayout(welcome_layout);
	this->node_info_panel->setLayout(node_info_layout);
	this->edge_info_panel->setLayout(edge_layout);
	this->search_panel->setLayout(search_layout);
	//Assemble global components
	global_line1->addWidget(this->panel_mode);
	global_line1->addWidget(this->view_slider, 1);
	global_line2->addWidget(this->display_mode);
	global_line2->addWidget(this->layout_strategy_combo, 1);
	this->global_layout->addLayout(global_line1);
	this->global_layout->addLayout(global_line2);
	this->global_layout->addWidget(welcome_panel);
	//Apply global layout
	this->setLayout(this->global_layout);

	//* Connect signals and slots
	//Global components
	connect(this->panel_mode, SIGNAL(clicked()), this, SLOT(switchMode()));
	connect(this->view_slider, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChangeWrapper(int)));
	connect(this->layout_strategy_combo, SIGNAL(activated(int)), this, SLOT(layoutChangeWrapper(int)));
	//Node info panel
	connect(this->topic_document_list, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(showDocument(QListWidgetItem*)));
	//Edge info panel
	connect(this->go_to_node1, SIGNAL(clicked()), this, SLOT(goToNodeA()));
	connect(this->go_to_node2, SIGNAL(clicked()), this, SLOT(goToNodeB()));
	//Search panel
	connect(this->search_btn, SIGNAL(clicked()), this, SLOT(searchNodes()));
	connect(this->clear_cond_btn, SIGNAL(clicked()), this, SLOT(clearSearchCond()));
	connect(this->new_cond_btn, SIGNAL(clicked()), this, SLOT(addSearchCond()));
	connect(this->search_result, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(showNodeInfoInSearch(QListWidgetItem*)));

	//* Other works
	//Set current panel
	this->current_panel = this->welcome_panel;
	//Set topic graph reference
	this->graph = _graph;
	//Normal mode by default
	this->search_mode = false;
}

//Destructor
TopicPanel::~TopicPanel()
{
	this->current_panel->setParent(NULL);
	delete this->welcome_panel;
	delete this->search_panel;
	delete this->node_info_panel;
	delete this->edge_info_panel;
	delete this->doc_dialog;
}

//Slots
//Show node information
void TopicPanel::showNodeInfo(unsigned long long _node)
{
	Node* node = reinterpret_cast<Node*>(_node);
	QList<QString> topic_words = node->prop<QList<QString>>("topicWords");
	QList<unsigned int> topic_docs = node->prop<QList<unsigned int>>("topicDocuments");

	this->topic_word_list->clear();
	this->topic_document_list->clear();

	this->node_info_title->setText(NODE_TITLE.arg(node->prop<unsigned int>("topic_id")));
	for (auto word : topic_words)
		this->topic_word_list->addItem(word);
	for (auto doc_id : topic_docs)
	{
		QListWidgetItem* doc_item = new QListWidgetItem("#" + QString::number(doc_id));
		doc_item->setData(DOC_ID, doc_id);
		this->topic_document_list->addItem(doc_item);
	}

	this->global_layout->removeWidget(this->current_panel);
	this->current_panel->setParent(NULL);
	this->global_layout->addWidget(this->node_info_panel);
	this->current_panel = this->node_info_panel;

	//Extra work for search mode
	if (this->search_mode)
	{
		this->search_mode = false;
		this->panel_mode->setText(TO_SEARCH_MODE);
		this->panel_mode->adjustSize();
	}
}

//Show edge information
void TopicPanel::showEdgeInfo(unsigned long long _edge)
{
	Edge* edge = reinterpret_cast<Edge*>(_edge);
	unsigned int node_a_id, node_b_id;
	double edge_weight = edge->prop<double>("weight");

	this->node_a = edge->getNodeA();
	this->node_b = edge->getNodeB();
	node_a_id = this->node_a->prop<unsigned int>("topic_id");
	node_b_id = this->node_b->prop<unsigned int>("topic_id");

	this->edge_msg->setText(EDGE_MSG_NON_DIRECTED.arg(node_a_id).arg(node_b_id).arg(edge_weight));
	this->go_to_node1->setText(GO_TO_NODE.arg(node_a_id));
	this->go_to_node2->setText(GO_TO_NODE.arg(node_b_id));

	this->global_layout->removeWidget(this->current_panel);
	this->current_panel->setParent(NULL);
	this->global_layout->addWidget(this->edge_info_panel);
	this->current_panel = this->edge_info_panel;

	//Extra work for search mode
	if (this->search_mode)
	{
		this->search_mode = false;
		this->panel_mode->setText(TO_SEARCH_MODE);
		this->panel_mode->adjustSize();
	}
}

//[Node info panel] Show document
void TopicPanel::showDocument(QListWidgetItem* item)
{
	unsigned int doc_id = item->data(DOC_ID).toUInt();
	TopicGraph::Document doc = this->graph->documents[doc_id];

	this->doc_dialog->showDocument(doc_id, doc.title, doc.content);
}

//[Edge info panel] Go to node A
void TopicPanel::goToNodeA()
{
	this->showNodeInfo(reinterpret_cast<unsigned long long>(this->node_a));
}

//[Edge info panel] Go to node B
void TopicPanel::goToNodeB()
{
	this->showNodeInfo(reinterpret_cast<unsigned long long>(this->node_b));
}

//[Global components] Scale change handler
void TopicPanel::scaleChangeHandler(double scale_value)
{
	this->view_slider->setValue((int)(scale_value * SLIDER_FACTOR));
}

//[Global components] Slider value change wrapper
void TopicPanel::sliderValueChangeWrapper(int value)
{
	emit scaleChanged((double)value / SLIDER_FACTOR);
}

//[Global components] Layout change wrapper
void TopicPanel::layoutChangeWrapper(int index)
{
	QTuple layout_tuple = this->layout_strategy_combo->itemData(index).value<QTuple>();
	emit layoutChanged(layout_tuple[0].toString(), layout_tuple[1].toBool());
}

//[Global components] Switch between normal mode and search mode
void TopicPanel::switchMode()
{
	//Switch to normal mode
	if (this->search_mode)
	{
		this->search_mode = false;

		this->global_layout->removeWidget(this->current_panel);
		this->current_panel->setParent(NULL);
		this->global_layout->addWidget(this->previous_panel);
		this->current_panel = this->previous_panel;

		this->panel_mode->setText(TO_SEARCH_MODE);
		this->panel_mode->adjustSize();
	}
	//Switch to search mode
	else
	{
		this->search_mode = true;
		//Store current panel
		this->previous_panel = this->current_panel;

		this->global_layout->removeWidget(this->current_panel);
		this->current_panel->setParent(NULL);
		this->global_layout->addWidget(this->search_panel);
		this->current_panel = this->search_panel;

		this->panel_mode->setText(TO_NORMAL_MODE);
		this->panel_mode->adjustSize();
	}
}

//[Search panel] Initialize Condition parsers
void TopicPanel::initCondParsers()
{
	//Topic ID
	TopicPanel::cond_parsers["topic_id"] = [](Node* node, QString cond) -> QVariant
	{
		bool success;
		unsigned int topic_id = cond.toUInt(&success);

		if (success)
			return (node->__prop__["topic_id"] == topic_id);
		else
			return QString("You enter an illegal topic ID!");
	};
	//Topic documents
	TopicPanel::cond_parsers["topicDocuments"] = [](Node* node, QString cond) -> QVariant
	{
		bool success;
		unsigned int doc_id = cond.toUInt(&success);

		if (success)
			return node->prop<QList<unsigned int>>("topicDocuments").contains(doc_id);
		else
			return QString("You enter an illegal topic document ID!");
	};
	//Topic words
	TopicPanel::cond_parsers["topicWords"] = [](Node* node, QString cond) -> QVariant
	{
		return node->prop<QList<QString>>("topicWords").contains(cond);
	};
}

//[Search panel] Add a search condition
void TopicPanel::addSearchCond()
{
	unsigned int row_count = this->search_cond_table->rowCount();

	//Insert a new row and add components
	this->search_cond_table->insertRow(row_count);
	//Add components
	auto prop_combo = new QComboBox();
	prop_combo->addItem("Topic ID", "topic_id");
	prop_combo->addItem("Topic Document", "topicDocuments");
	prop_combo->addItem("Topic Word", "topicWords");
	this->search_cond_table->setCellWidget(row_count, 0, prop_combo);
	this->search_cond_table->setCellWidget(row_count, 1, new QLineEdit());
}

//[Search panel] Clear search conditions
void TopicPanel::clearSearchCond()
{
	unsigned int row_count = this->search_cond_table->rowCount();
	while (row_count > 0)
	{
		this->search_cond_table->removeRow(row_count - 1);
		row_count--;
	}
}

//[Search panel] Search for nodes
void TopicPanel::searchNodes()
{
	QList<QPair<QString, QList<QString>>> search_cond;
	unsigned int row_count = this->search_cond_table->rowCount();
	unsigned int i;
	bool search_failed = false;
	QString failed_msg;

	//Preprocess search condition
	for (i = 0; i < row_count; i++)
	{
		auto cond_prop_combo = (QComboBox*)(this->search_cond_table->cellWidget(i, 0));
		QString cond_prop = cond_prop_combo->itemData(cond_prop_combo->currentIndex()).toString();
		QString raw_cond = ((QLineEdit*)(this->search_cond_table->cellWidget(i, 1)))->text();
		
		search_cond.append(QPair<QString, QList<QString>>(cond_prop, raw_cond.split(' ', QString::SkipEmptyParts)));
	}

	//Search for nodes
	auto result_list = this->graph->getNodes([&search_cond, &search_failed, &failed_msg](Node* node)
	{
		if (search_failed)
			return false;

		bool result = true;
		for (auto cond_pair : search_cond)
		{
			bool tmp_result = false;
			for (auto cond : cond_pair.second)
			{
				QVariant single_result = (TopicPanel::cond_parsers[cond_pair.first])(node, cond);
				if (single_result.type() == QVariant::Bool)
					tmp_result = tmp_result || single_result.toBool();
				else
				{
					search_failed = true;
					failed_msg = single_result.toString();
					return false;
				}
			}
			result = result && tmp_result;
		}

		return result;
	});

	//Show node ID as result if succeeded
	if (!search_failed)
	{
		this->search_result->clear();
		for (auto node : result_list)
		{
			auto result_item = new QListWidgetItem("#" + QString::number(node->prop<unsigned int>("topic_id")));
			result_item->setData(SEARCH_RESULT_NODE_REF, reinterpret_cast<unsigned long long>(node));
			this->search_result->addItem(result_item);
		}
	}
	//Failed, prompt and exit
	else
		QMessageBox::information(NULL, "Search Failed", "Error occured when attempting to search.\n" + failed_msg);
}

//[Search panel] Show node information
void TopicPanel::showNodeInfoInSearch(QListWidgetItem* item)
{
	unsigned long long _node = item->data(SEARCH_RESULT_NODE_REF).toULongLong();
	this->showNodeInfo(_node);
	emit highlightNode(_node);
}